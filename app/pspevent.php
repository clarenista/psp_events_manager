<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Rate;
use App\AdditionalFee;
use App\Accommodation;
use App\Voucher;
use App\payment_details;

class Pspevent extends Model
{
    function rates(){
    	return $this->hasMany(Rate::class, 'pspevent_id', 'id')->orderBy('created_at', 'ASC');
    }

    function additional_fees(){
    	return $this->hasMany(AdditionalFee::class, 'pspevent_id', 'id')->orderBy('created_at', 'ASC');
    }

    function accommodations(){
    	return $this->hasMany(Accommodation::class, 'pspevent_id', 'id');
    }

    function vouchers(){
    	return $this->hasMany(Voucher::class, 'pspevent_id', 'id');
        
    }
    
    function payment_details(){
        return $this->hasMany(payment_details::class, 'pspevent_id', 'id');

    }

    
}
