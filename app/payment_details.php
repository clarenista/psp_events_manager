<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Rate;
use App\AdditionalFee;
use App\pspevent;

class payment_details extends Model
{
    function member_details(){
    	return $this->hasOne(User::class, 'id', 'user_id')->select('id', 'first_name', 'last_name', 'classification', 'email', 'affiliation');
    }

    function rate_details(){
    	return $this->hasOne(Rate::class, 'id', 'rate_id');
    }

    function additional_fee_details(){
    	return $this->hasMany(AdditionalFee::class, 'id', 'additional_fee_id');
    }

    function event_details(){
    	return $this->belongsTo(pspevent::class, 'pspevent_id', 'id');
    }

}
