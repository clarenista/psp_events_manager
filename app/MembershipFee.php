<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MembershipFee extends Model
{
    protected $fillable = ['name', 'amount'];
}
