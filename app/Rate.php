<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Pspevent;

class Rate extends Model
{
    public function event(){
    	return $this->hasOne(Pspevent::class, 'pspevent_id', 'id')->select('id', 'title');
    }
}
