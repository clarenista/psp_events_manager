<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\AreasOfPractice;

class C19OCRegistrant extends Model
{
    public function areasOfPractice(){
    	return $this->hasMany(AreasOfPractice::class, 'registrant_id', 'id');
    }
}
