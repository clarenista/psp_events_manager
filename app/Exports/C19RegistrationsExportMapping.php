<?php

namespace App\Exports;

use App\C19OCRegistrant;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithEvents;
use Carbon\Carbon;

class C19RegistrationsExportMapping implements FromCollection, WithMapping, WithHeadings, ShouldAutoSize,WithEvents
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return C19OCRegistrant::with('areasOfPractice')->orderBy('created_at', 'asc')->get();
    }

    public function map($registration) : array {
    	$row = "";
    	foreach ($registration->areasOfPractice as $area) {
    		$row = $row.$area->area." \n";
    	}
        return [
            $registration->fname,
            $registration->mname,
            $registration->lname,
            $registration->email,
            $registration->contact,
            $registration->dob,
            $registration->specialty == 'ac' ? 'AP-CP' : 'CP',
            self::renderClassification($registration->classification),
            $registration->affiliation,
            $registration->purpose,
            $row,
            self::renderChapterMembership($registration->cMembership),
            Carbon::parse($registration->created_at)
        ] ;
 
 
    }   

    public function headings() : array {
        return [
           'First Name',
           'Middle Name',
           'Family Name',
           'Email Address',
           'Contact Number',
           'Date of Birth',
           'Specialty',
           'Classification',
           'Affiliation',
           'Purpose',
           'Area of Practice',
           'Chapter Membership',
           'Created At'
        ] ;
    }    

    public function registerEvents(): array
    {
        return [
            AfterSheet::class    => function(AfterSheet $event) {
                $cellRange = 'K2:K5000'; 
				$event->sheet->getDelegate()->getStyle($cellRange)->getAlignment()->setWrapText(true);
            },
        ];
    }     

    private function renderClassification($classification){
      switch ($classification) {
        case 'F':
          return "Fellow";
          break;
        case 'D':
          return "Diplomate";
          break;  
        default:
          return "Resident";
          break;
      }
    }
    private function renderChapterMembership($cMembership){
      switch ($cMembership) {
        case 'clc':
          return "Central Luzon Chapter";
          break;
        case 'nlc':
          return "North Luzon Chapter";
          break;
        case 'slc':
          return "South Luzon Chapter";
          break;
        case 'bic':
          return "Bicol Chapter";
          break;
        case 'cvc':
          return "Central Visayas Chapter";
          break;
        case 'wvc':
          return "Western Visayas Chapter";
          break;
        case 'nmc':
          return "Northern Mindanao Chapter";
          break;
        case 'smc':
          return "Southern Mindanao Chapter";
          break;
        case 'swmc':
          return "South Western Mindanao Chapter";
          break;                                                            
        default:
          return "None";
          break;
      }
    }
}
