<?php

namespace App\Exports;

use App\User;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithEvents;
use Carbon\Carbon;

class UsersExport implements FromCollection, WithMapping, WithHeadings, ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return User::with('payment_detail.rate_details', 'payment_detail.additional_fee_details')->withCount('payment_details')->having('payment_details_count', '>', 0)->get();
    }
    public function map($registration) : array {
    	$row = "";
        if($registration->payment_detail->additional_fee_details != null){

            foreach ($registration->payment_detail->additional_fee_details as $additional) {
                $row = $row.$additional->classification ." - ".$additional->amount." \n";
            }
        }
        return [
            $registration->first_name,
            $registration->last_name,
            $registration->contact,
            $registration->email,
            $registration->affiliation,
            $registration->username,
            $registration->classification,
            $registration->payment_detail->rate_details->category." - ".$registration->payment_detail->rate_details->amount,
            $row,
            $registration->payment_detail->discount_percent,
            $registration->payment_detail->total,
            Carbon::parse($registration->created_at)
        ] ;
 
 
    }   
    public function headings() : array {
        return [
           'First Name',
           'Last Name',
           'Contact Number',
           'Email Address',
           'Affiliation',
           'Username',
           'Classification',
           'Rate',
           'Additional Fee',
           'Discount',
           'Total',
           'Date Added'
        ] ;
    }    
}
