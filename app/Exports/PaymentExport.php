<?php

namespace App\Exports;

use App\payment_details;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithEvents;
use Carbon\Carbon;

class PaymentExport implements FromCollection, WithMapping, WithHeadings, ShouldAutoSize
{
    /**
     * @return \Illuminate\Support\Collection
     */

    protected $id;

    function __construct($id)
    {
        $this->id = $id;
    }
    public function collection()
    {
        return payment_details::where('pspevent_id', $this->id)->get();
    }

    public function map($payment): array
    {
        return [
            $payment->member_details->id,
            $payment->member_details->first_name,
            $payment->member_details->last_name,
            $payment->member_details->email,
            $payment->member_details->classification,
            $payment->member_details->affiliation,

            $payment->rate_details->category,

            $payment->is_accepted != 1 ? "PENDING" : "PAID",
            $payment->discount_percent ? $payment->discount_percent : "N/A",
            $payment->total,
            Carbon::parse($payment->created_at)
        ];
    }
    public function headings(): array
    {
        return [
            'ID',
            'First Name',
            'Last Name',
            'Email Address',
            'Classification',
            'Affiliation',
            'Rate Selected',
            'Payment Status',
            'Discount',
            'Total',
            'Date Added'
        ];
    }
}
