<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class VoucherController extends Controller
{
    function store(Request $request, $event_id){
		$voucher = DB::table('vouchers')
    			->insert([
    				'pspevent_id' => $event_id,
    				'code' => $request->code,
    				'discount' => $request->discount,
    				'created_at' => \Carbon\Carbon::now(),
    				'updated_at' => \Carbon\Carbon::now(),
    			]);
    	$id = DB::getPdo()->lastInsertId();

    	return response()->json(['status'=>'success', 'id' => $id]);    	
    }

    function show($event_id,$voucher_id){
    	$voucher_details = DB::table('vouchers')
    					->where('id', $voucher_id)
    					->first();
   		$voucher_details->event = DB::table('pspevents')->where('id', $event_id)->select('id')->first();
    	return view('vouchers.show', ['voucher_details' => json_encode($voucher_details)]);
    	// return response()->json(['rate_details' => $rate_details]);

    }    

    function update(Request $request, $event_id, $voucher_id){
        $voucher = DB::table('vouchers')
                ->where('id', $voucher_id)
                ->update([
                    'code' => $request->code,
                    'discount' => $request->discount,
                    'updated_at' => \Carbon\Carbon::now(),                  
                ]);
        return response()->json(['status' => 'success']);
    }    

    function checkCode(Request $request){
        $isCodeExists = DB::table('vouchers')->where('pspevent_id', $request->pspevent_id)->where('code', $request->voucher_code)->first();
        if($isCodeExists){
            return response()->json(['isCodeExists' => 'true', 'discount' => $isCodeExists->discount]);
        }else{
            return response()->json(['isCodeExists' => 'false']);

        }
    }
}
