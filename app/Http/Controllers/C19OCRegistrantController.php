<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\C19OCRegistrant;
use App\AreasOfPractice;
use App\Setting;
use Carbon\Carbon;
use App\Exports\C19RegistrationsExportMapping;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Mail;

class C19OCRegistrantController extends Controller
{
    public function index(){
        $settings = Setting::find(1);
        $start_dt = Carbon::parse(date($settings->c19_start_period));
        $end_dt = Carbon::parse(date($settings->c19_end_period));
        // dd($end_dt->lessThan(Carbon::parse(date('Y-m-d H:i:s'))));
        // dd($start_dt->lessThan(Carbon::parse(date('Y-m-d H:i:s'))));
        if($start_dt->lessThan(Carbon::parse(date('Y-m-d H:i:s')))){
            if($end_dt->greaterThan(Carbon::parse(date('Y-m-d H:i:s')))){
               return view('covdOnlineCourse.index', ['settings' => $settings]);
            }else{
                return 'Registration is closed.';
            }
    	   return view('covdOnlineCourse.index', ['settings' => $settings]);
        }else{
            return 'Registration will open on '.Carbon::parse(date($settings->c19_start_period))->toDayDateTimeString();
        }

    }

    public function residentsIndex(){
        $settings = Setting::find(1);
        $start_dt = Carbon::parse(date($settings->c19_start_period));
        $end_dt = Carbon::parse(date($settings->c19_end_period));
        // dd($end_dt->lessThan(Carbon::parse(date('Y-m-d H:i:s'))));
        // dd($start_dt->lessThan(Carbon::parse(date('Y-m-d H:i:s'))));
        if($start_dt->lessThan(Carbon::parse(date('Y-m-d H:i:s')))){
            if($end_dt->greaterThan(Carbon::parse(date('Y-m-d H:i:s')))){
                return view('covdOnlineCourse.residents', ['settings' => $settings]);
            }else{
                return 'Registration is closed.';
            }
            return view('covdOnlineCourse.residents', ['settings' => $settings]);
        }else{
            return 'Registration will open on '.Carbon::parse(date($settings->c19_start_period))->toDayDateTimeString();
        }

    }    

    public function saveRegistrant(Request $request){
        $settings = Setting::find(1);
        $start_dt = Carbon::parse(date($settings->c19_start_period));
        $end_dt = Carbon::parse(date($settings->c19_end_period));
        // dd($end_dt->lessThan(Carbon::parse(date('Y-m-d H:i:s'))));
        // dd($start_dt->lessThan(Carbon::parse(date('Y-m-d H:i:s'))));
        if($start_dt->lessThan(Carbon::parse(date('Y-m-d H:i:s')))){
            if($end_dt->greaterThan(Carbon::parse(date('Y-m-d H:i:s')))){
                $exists = C19OCRegistrant::where('fname', $request->fname)->where('lname', $request->lname)->exists();
                if(!$exists){
                    try{
                        $registrant = new C19OCRegistrant();
                        $registrant->fname = ucwords($request->fname);
                        $registrant->mname = ucwords($request->mname);
                        $registrant->lname = ucwords($request->lname);
                        $registrant->email = $request->email;
                        $registrant->contact = $request->contact;
                        $registrant->dob = $request->dob;
                        $registrant->specialty = $request->specialty;
                        $registrant->classification = $request->classification;
                        $registrant->affiliation = $request->affiliation;
                        $registrant->cMembership = $request->cMembership;
                        $registrant->purpose = $request->purpose;
                        $registrant->triedOnlineCourse = $request->triedOnlineCourse;
                        $registrant->levelsOfComputerLiteracy = $request->levelsOfComputerLiteracy;
                        $registrant->save();

                        $id = $registrant->id;
                        $areas = explode(',', $request->aop);
                        for ($i=0; $i < count($areas) ; $i++) { 
                            $aop = new AreasOfPractice();
                            $aop->registrant_id = $id;
                            $aop->area = $areas[$i];
                            $aop->save();
                        }

                        return response()->json(['status' => 'success']);
                    }
                    catch(\Exception $e){
                        return $e;
                    }
                    
                }else{
                    return response()->json(['status' => 'failed', 'message' => 'Are you registering for your self? Your name had been enlisted already.']);

                }
            }else{
                return 'Registration is closed';
            }
            $registrant = new C19OCRegistrant();
            $registrant->fname = $request->fname;
            $registrant->mname = $request->mname;
            $registrant->lname = $request->lname;
            $registrant->email = $request->email;
            $registrant->contact = $request->contact;
            $registrant->dob = $request->dob;
            $registrant->specialty = $request->specialty;
            $registrant->classification = $request->classification;
            $registrant->affiliation = $request->affiliation;
            $registrant->cMembership = $request->cMembership;
            $registrant->purpose = $request->purpose;
            $registrant->triedOnlineCourse = $request->triedOnlineCourse;
            $registrant->levelsOfComputerLiteracy = $request->levelsOfComputerLiteracy;
            $registrant->save();

            $id = $registrant->id;
            $areas = explode(',', $request->aop);
            for ($i=0; $i < count($areas) ; $i++) { 
                $aop = new AreasOfPractice();
                $aop->registrant_id = $id;
                $aop->area = $areas[$i];
                $aop->save();
            }

            return response()->json(['status' => 'success']);
        }else{
            return 'Registration is not yet ready';
        }        



    }
   
    public function saveResidentRegistrant(Request $request){
        $settings = Setting::find(1);
        $start_dt = Carbon::parse(date($settings->c19_start_period));
        $end_dt = Carbon::parse(date($settings->c19_end_period));
        // dd($end_dt->lessThan(Carbon::parse(date('Y-m-d H:i:s'))));
        // dd($start_dt->lessThan(Carbon::parse(date('Y-m-d H:i:s'))));
        if($start_dt->lessThan(Carbon::parse(date('Y-m-d H:i:s')))){
            if($end_dt->greaterThan(Carbon::parse(date('Y-m-d H:i:s')))){
                $exists = C19OCRegistrant::where('fname', $request->fname)->where('lname', $request->lname)->exists();
                if(!$exists){
                    try{
                        $registrant = new C19OCRegistrant();
                        $registrant->fname = ucwords($request->fname);
                        $registrant->mname = ucwords($request->mname);
                        $registrant->lname = ucwords($request->lname);
                        $registrant->email = $request->email;
                        $registrant->contact = $request->contact;
                        $registrant->dob = $request->dob;
                        $registrant->classification = $request->classification;
                        $registrant->specialty = $request->specialty ? $request->specialty : '';
                        $registrant->affiliation = $request->affiliation ? $request->affiliation : '';
                        $registrant->year_level = $request->year_level;
                        $registrant->institution = $request->institution;
                        $registrant->purpose = $request->purpose;
                        $registrant->triedOnlineCourse = $request->triedOnlineCourse;
                        $registrant->levelsOfComputerLiteracy = $request->levelsOfComputerLiteracy;
                        $registrant->save();

                        return response()->json(['status' => 'success']);
                    }
                    catch(\Exception $e){
                        return $e;
                    }
                    
                }else{
                    return response()->json(['status' => 'failed', 'message' => 'Are you registering for your self? Your name had been enlisted already.']);

                }
            }else{
                return 'Registration is closed';
            }
            // $registrant = new C19OCRegistrant();
            // $registrant->fname = $request->fname;
            // $registrant->mname = $request->mname;
            // $registrant->lname = $request->lname;
            // $registrant->email = $request->email;
            // $registrant->contact = $request->contact;
            // $registrant->dob = $request->dob;
            // $registrant->specialty = $request->specialty;
            // $registrant->classification = $request->classification;
            // $registrant->institution = $request->institution;
            // $registrant->cMembership = $request->cMembership;
            // $registrant->purpose = $request->purpose;
            // $registrant->triedOnlineCourse = $request->triedOnlineCourse;
            // $registrant->levelsOfComputerLiteracy = $request->levelsOfComputerLiteracy;
            // $registrant->save();

            // $id = $registrant->id;
            // $areas = explode(',', $request->aop);
            // for ($i=0; $i < count($areas) ; $i++) { 
            //     $aop = new AreasOfPractice();
            //     $aop->registrant_id = $id;
            //     $aop->area = $areas[$i];
            //     $aop->save();
            // }

            // return response()->json(['status' => 'success']);
        }else{
            return 'Registration is not yet ready';
        }        



    }
    

    private function storeRegistrant($request){
        $registrant = new C19OCRegistrant();
        $registrant->fname = $request->fname;
        $registrant->mname = $request->mname;
        $registrant->lname = $request->lname;
        $registrant->email = $request->email;
        $registrant->contact = $request->contact;
        $registrant->dob = $request->dob;
        $registrant->specialty = $request->specialty;
        $registrant->classification = $request->classification;
        $registrant->affiliation = $request->affiliation;
        $registrant->cMembership = $request->cMembership;
        $registrant->purpose = $request->purpose;
        $registrant->triedOnlineCourse = $request->triedOnlineCourse;
        $registrant->levelsOfComputerLiteracy = $request->levelsOfComputerLiteracy;
        $registrant->save();

        $id = $registrant->id;
        $areas = explode(',', $request->aop);
        for ($i=0; $i < count($areas) ; $i++) { 
            $aop = new AreasOfPractice();
            $aop->registrant_id = $id;
            $aop->area = $areas[$i];
            $aop->save();
        }

        return response()->json(['status' => 'success']);
    }

    public function registrants(){
        $settings = Setting::find(1);
        $registrants = C19OCRegistrant::with('areasOfPractice')->orderBy('created_at', 'desc')->get();
        return view('covdOnlineCourse.registrants', ['registrants' => $registrants, 'settings' => $settings]);
    }

    public function export_mapping() {
        return Excel::download( new C19RegistrationsExportMapping(), 'c19registrations.xlsx') ;
    }    

    public function email(){
        $registrants = C19OCRegistrant::where('is_emailed', '0')->get();
        foreach ($registrants as $key => $value) {

            $registrant = C19OCRegistrant::find($value->id);
            if($registrant->is_emailed == 0){
                // email registrant
                $to_email = $registrant->email;
                $data = array("registrant" => $registrant,);
                Mail::send('emails.c19mail', $data, function($message) use ($to_email) {
                    $message->to($to_email)
                            ->subject('Acceptance to the Online Molecular Pathology Course');
                    $message->from('molepath.ecourse@gmail.com','Acceptance to the Online Molecular Pathology Course');
                });                    
                $registrant->is_emailed = 1;
                $registrant->save();
            }
        }
    }
}
