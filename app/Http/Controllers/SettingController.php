<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Setting;
use App\MembershipFee;
use Carbon\Carbon;
use PDO;

class SettingController extends Controller
{
	public function fetchSettings()
	{
		return response()->json(['settings' => Setting::find(1)]);
	}

	public function update(Request $request)
	{
		$settings = Setting::find(1);
		$settings->c19_start_period = Carbon::parse($request->date_startPeriod);
		$settings->c19_end_period = Carbon::parse($request->date_endPeriod);
		$settings->save();
		return 'success';
	}

	function psp()
	{
		$membershipFees = MembershipFee::all();
		return view('settings.psp.index', compact('membershipFees'));
	}

	function create_mf(MembershipFee $membershipFee)
	{
		return view('settings.psp.createMf', compact('membershipFee'));
	}

	function store_mf(Request $request)
	{
		$membershipFee = MembershipFee::create($request->except('_token'));
		return redirect(\route('mf_index'));
	}

	function edit_mf($id)
	{
		$membershipFee = MembershipFee::find($id);
		return view('settings.psp.createMf', compact('membershipFee'));
	}

	function update_mf(Request $request, $id)
	{
		$membershipFee = MembershipFee::find($id);
		$membershipFee->update($request->except('_token'));
		return redirect(\route('mf_index'));
	}

	function delete_mf($id)
	{
		MembershipFee::destroy($id);
		return redirect(\route('mf_index'));
	}
}
