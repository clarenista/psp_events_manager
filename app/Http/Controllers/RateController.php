<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Rate;

class RateController extends Controller
{
    function store(Request $request, $event_id){
    	$rate = DB::table('rates')
    			->insert([
    				'pspevent_id' => $event_id,
                    'currency' => $request->currency,
    				'amount' => $request->amount,
    				'category' => $request->category,
    				'sub_category' => $request->sub_category,
    				'created_at' => \Carbon\Carbon::now(),
    				'updated_at' => \Carbon\Carbon::now(),
    			]);
    	$id = DB::getPdo()->lastInsertId();

    	return response()->json(['status'=>'success', 'id' => $id]);
    }

    function show($event_id,$rate_id){
    	$rate_details = DB::table('rates')
    					->where('id', $rate_id)
    					->first();
   		$rate_details->event = DB::table('pspevents')->where('id', $event_id)->select('id')->first();
    	return view('rates.show', ['rate_details' => json_encode($rate_details)]);
    	// return response()->json(['rate_details' => $rate_details]);

    }

	function update(Request $request, $event_id, $rate_id){
		$rate = DB::table('rates')
				->where('id', $rate_id)
				->update([
					'currency' => $request->currency,
					'amount' => $request->amount,
					'category' => $request->category,
					'sub_category' => $request->sub_category,
					'updated_at' => \Carbon\Carbon::now(),					
				]);
		return response()->json(['status' => 'success']);
	}
}
