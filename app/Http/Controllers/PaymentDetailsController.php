<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Accommodation;
use App\payment_details;
use Illuminate\Support\Facades\Mail;
use DateTime;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Psr7;
use Illuminate\Support\Facades\Storage;
use App\Exports\UsersExport;
use App\Exports\PaymentExport;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Str;

class PaymentDetailsController extends Controller
{
	const ACCESS_KEY = '7b56984a6d793c2896129573f058500a';
	const PROFILE_ID = '6A500F76-65EC-4D40-8BCC-2C9D7C6DE40D';
	const SECRET_KEY = 'ade2223834e249eaa28e1d6043b41376a8379039f2be4b2f8ab95b5e0fce87acfd41fed6117142188530dce3614f57ad018b1c8c4c3b4c349e721ee6f9dab1e30266886a71da442dacc16fd08a83126e805ce6a6524e4405b837048bf5d9e9139fc9c2e4fc4f4b2db5e2c501f0fc2bc565aebdb58512403aa23c121943ad6a13';
	const CURRENCY = 'PHP';
	const TRANSACTION_TYPE = 'sale';
	const HMAC_SHA256 = 'sha256';
	const LOCALE = 'en';

	private function sign($params)
	{
		return $this->signData($this->buildDataToSign($params), self::SECRET_KEY);
	}

	private function signData($data, $secretKey)
	{
		return base64_encode(hash_hmac('sha256', $data, $secretKey, true));
	}

	private function buildDataToSign($params)
	{
		$signedFieldNames = explode(",", $params["signed_field_names"]);
		foreach ($signedFieldNames as $field) {
			$dataToSign[] = $field . "=" . $params[$field];
		}
		return self::commaSeparate($dataToSign);
	}

	private function commaSeparate($dataToSign)
	{
		return implode(",", $dataToSign);
	}

	public function proceed_registration(Request $request, $event_id)
	{
		$url = url()->previous();
		$contains = Str::contains($url, '/administrator-register-user');
		// try {
		//     $client = new \GuzzleHttp\Client();
		//     $url = "https://testsecureacceptance.cybersource.com/checkout";
		//     $checkout_url = "https://testsecureacceptance.cybersource.com/checkout";

		//     $date = new DateTime();
		//     $request->request->add(['access_key' => self::ACCESS_KEY]);
		//     $request->request->add(['profile_id' => self::PROFILE_ID]);
		//     $request->request->add(['transaction_uuid' => uniqid()]);
		//     $request->request->add(['signed_field_names' => $request->signed_field_names]);
		//     $request->request->add(['unsigned_field_names' => '']);
		//     $request->request->add(['signed_date_time' => gmdate("Y-m-d\TH:i:s\Z")]);
		//     $request->request->add(['locale' => self::LOCALE]);
		//     $request->request->add(['transaction_type' => self::TRANSACTION_TYPE]);
		//     $request->request->add(['reference_number' => $date->getTimestamp()]);
		//     $request->request->add(['auth_trans_ref_no' => $date->getTimestamp()]);
		//     $request->request->add(['amount' => number_format((float)$request->total, 2, '.', '')]);
		//     $request->request->add(['currency' => self::CURRENCY]);
		//     $request->request->add(['bill_to_forename' => Auth::user()->first_name]);
		//     $request->request->add(['bill_to_surname' => Auth::user()->last_name]);
		//     $request->request->add(['submit' => 'Submits']);
		//     $request->request->add(['signature' => $this->sign($request->all())]);
		//     // $myBody['access_key'] = self::ACCESS_KEY;
		//     // $myBody['profile_id'] = self::PROFILE_ID;
		//     // $myBody['transaction_uuid'] = uniqid();
		//     // $myBody['signed_field_names'] = $request->signed_field_names;
		//     // $myBody['unsigned_field_names'] = '';
		//     // $myBody['signed_date_time'] = gmdate("Y-m-d\TH:i:s\Z");
		//     // $myBody['locale'] = self::LOCALE;
		//     // $myBody['transaction_type'] = self::TRANSACTION_TYPE;
		//     // $myBody['reference_number'] = $date->getTimestamp();
		//     // $myBody['auth_trans_ref_no'] = $date->getTimestamp();
		//     // $myBody['amount'] = number_format((float)$request->total, 2, '.', '');
		//     // $myBody['currency'] = self::CURRENCY;
		//     // $myBody['bill_to_forename'] = Auth::user()->first_name;
		//     // $myBody['bill_to_surname'] = Auth::user()->last_name;
		//     // $myBody['submit'] = 'Submits';
		//     // $myBody['signature'] = $this->sign($myBody);
		//     // $post_request = $client->post($url,  ['form_params'=>$myBody]);
		//     $response = $client->request('POST', $url, ['form_params'=>$request->all()]);
		//     // return redirect()->away($checkout_url);
		// 	// dd($myBody);
		//     dd($response);
		//     // return view('redirect')->with('requests', $myBody);
		//     // return redirect('redirect')->with('requests', implode("||", $myBody));
		//     // return response()->json(['status'=>'success','redirect_page' => $url, 'requests'=>$myBody]);
		// } catch (ClientException $e) {
		//     echo Psr7\str($e->getRequest());
		//     echo Psr7\str($e->getResponse());
		// }	
		$user_id = Auth::user()->role == 3 ? $request->user_id : Auth::id();
		$event = DB::table('pspevents')->where('id', $event_id)->select('id', 'title', 'end_registration')->first();
		$end_dt = \Carbon\Carbon::parse(date($event->end_registration));
		$end_dt = \Carbon\Carbon::parse(date($end_dt));

		if (Auth::user()->role == 3) {
			$payment_method = $request->payment_method;
			$rate_id = $request->rate_id;
			$accommodation_id = $request->accommodation_id;
			$total = $request->total;
			$imageName = $request->file == null ? '' : 'event_' . $event_id . '-user_' . $user_id . '.' . $request->file->getClientOriginalExtension();
			if ($request->file != null) {

				$request->file->move(public_path('uploads/sc_id'), $imageName);
			}
			$payment_details = DB::table('payment_details')
				->insert([
					'pspevent_id' => $event_id,
					'user_id' => $user_id,
					'rate_id' => $rate_id,
					'accommodation_id' => $accommodation_id  != 'null' ? $accommodation_id : 0,
					'sc_file_path' => $request->file  != null ? '/uploads/sc_id/' . 'event_' . $event_id . '-user_' . $user_id . '.' . $request->file->getClientOriginalExtension() : '',
					'payment_method' => $payment_method,
					'discount' => $request->discount,
					'discount_type' => $request->discount_type,
					'discount_percent' => $request->discount_percent,
					'total' => number_format((float)$request->total, 2, '.', ''),
					'additional_fee_id' => $request->additional_fee_id,
					'membership_fees_id' => $request->membership_fees_id,
					'created_at' => \Carbon\Carbon::now(),
					'updated_at' => \Carbon\Carbon::now(),
				]);

			if ($accommodation_id != '0') {
				$accommodation = Accommodation::find($accommodation_id);
				if ($accommodation->isLimited == 1) {
					$accommodation->limit = $accommodation->limit - 1;
					$accommodation->save();
				}
			}
		} else {
			if ($contains) {
				$payment_method = $request->payment_method;
				$rate_id = $request->rate_id;
				$accommodation_id = $request->accommodation_id;
				$total = $request->total;
				$imageName = $request->file == null ? '' : 'event_' . $event_id . '-user_' . $user_id . '.' . $request->file->getClientOriginalExtension();
				if ($request->file != null) {

					$request->file->move(public_path('uploads/sc_id'), $imageName);
				}
				$payment_details = DB::table('payment_details')
					->insert([
						'pspevent_id' => $event_id,
						'user_id' => $user_id,
						'rate_id' => $rate_id,
						'accommodation_id' => $accommodation_id  != 'null' ? $accommodation_id : 0,
						'sc_file_path' => $request->file  != null ? '/uploads/sc_id/' . 'event_' . $event_id . '-user_' . $user_id . '.' . $request->file->getClientOriginalExtension() : '',
						'payment_method' => $payment_method,
						'discount' => $request->discount,
						'discount_type' => $request->discount_type,
						'discount_percent' => $request->discount_percent,
						'total' => number_format((float)$request->total, 2, '.', ''),
						'additional_fee_id' => $request->additional_fee_id,
						'membership_fees_id' => $request->membership_fees_id,
						'created_at' => \Carbon\Carbon::now(),
						'updated_at' => \Carbon\Carbon::now(),
					]);
				$member = \App\User::find(Auth::id());
				// dd($request->total  0);
				if ($request->total > 0) {

					self::emailMemberRegistrationToEvent($member, $event, $request->root());
				}
				if ($accommodation_id != '0') {
					$accommodation = Accommodation::find($accommodation_id);
					if ($accommodation->isLimited == 1) {
						$accommodation->limit = $accommodation->limit - 1;
						$accommodation->save();
					}
				}
			} else {

				if ($end_dt->greaterThan(\Carbon\Carbon::parse(date('Y-m-d H:i:s')))) {
					$payment_method = $request->payment_method;
					$rate_id = $request->rate_id;
					$accommodation_id = $request->accommodation_id;
					$total = $request->total;
					$imageName = $request->file == null ? '' : 'event_' . $event_id . '-user_' . $user_id . '.' . $request->file->getClientOriginalExtension();
					if ($request->file != null) {

						$request->file->move(public_path('uploads/sc_id'), $imageName);
					}
					$payment_details = DB::table('payment_details')
						->insert([
							'pspevent_id' => $event_id,
							'user_id' => $user_id,
							'rate_id' => $rate_id,
							'accommodation_id' => $accommodation_id  != 'null' ? $accommodation_id : 0,
							'sc_file_path' => $request->file  != null ? '/uploads/sc_id/' . 'event_' . $event_id . '-user_' . $user_id . '.' . $request->file->getClientOriginalExtension() : '',
							'payment_method' => $payment_method,
							'discount' => $request->discount,
							'discount_type' => $request->discount_type,
							'discount_percent' => $request->discount_percent,
							'total' => number_format((float)$request->total, 2, '.', ''),
							'additional_fee_id' => $request->additional_fee_id,
							'membership_fees_id' => $request->membership_fees_id,
							'created_at' => \Carbon\Carbon::now(),
							'updated_at' => \Carbon\Carbon::now(),
						]);
					$member = \App\User::find(Auth::id());
					if ($request->total > 0) {

						self::emailMemberRegistrationToEvent($member, $event, $request->root());
					}

					if ($accommodation_id != '0') {
						$accommodation = Accommodation::find($accommodation_id);
						if ($accommodation->isLimited == 1) {
							$accommodation->limit = $accommodation->limit - 1;
							$accommodation->save();
						}
					}
				} else {
					return 'Registration is closed';
				}
			}
		}






		if ($payment_method == 'ushare') {
			// $merchant_code = env('UB_MERCHANT_CODE');
			// $api_id = env('UB_API_ID');
			// $api_secret = env('UB_API_SECRET');

			// $gmt_now = gmdate("M d Y H:i:s", time());
			// $gmt_now = strtotime($gmt_now);
			// $api_expires = $gmt_now + 28800 + 10800; //GMT + 8 Hours (Philippine Time) + expiration time in secs (in this case 3 hours - 3600*3 = 10800)

			// $api_sig = hash_hmac('sha256', $api_expires, $api_secret);

			// //prepare fields to populate: see documentation #api-methods-checkouts section -------------------------------------------------------------------
			// $fields = array();

			// $particular_fields = array(
			//                     'particular_name_1'     =>  'Registration for '.$event->title,

			//                     'particular_price_1'    =>  number_format((float)$request->total, 2, '.', ''),
			//                     'particular_quantity_1' =>  '1',
			//                     'particular_code_1'     =>  'REG_'.$event_id.'_USER_'.Auth::id()
			//                ); //required

			// $fields = array_merge($fields, $particular_fields); 

			// $autopopulate_fields = array('billing_first_name' => Auth::user()->first_name,
			//                              'billing_last_name'  => Auth::user()->last_name,
			//                              'billing_contact_no' => Auth::user()->contact,
			//                              'billing_email' => Auth::user()->email,
			//                              'billing_organization' => Auth::user()->affiliation); //optional


			// $fields = array_merge($fields, $autopopulate_fields);

			// $fields_string = '';

			// foreach($fields as $key=>$value) 
			// { 
			//     $fields_string .= $key.'='.$value.'&'; 
			// }
			// $fields_string = rtrim($fields_string,"&");

			// //prepare checkouts API call ---------------------------------------------------------------------------------------------------------------------
			// $checkouts_url = ''; //for PROD environment use https://secured.ubiz.unionbank.com.ph
			// if(env('UB_ENVIRONMENT') == 'PROD'){
			// 	$checkouts_url = 'https://secured.ubiz.unionbank.com.ph/v1/checkouts.json';
			// }else{
			// 	$checkouts_url = 'http://api.dev.ubiz.unionbank.com.ph/v1/checkouts.json';

			// }

			// $ch = curl_init();

			// curl_setopt($ch,CURLOPT_URL, $checkouts_url);
			// curl_setopt($ch, CURLOPT_HTTPHEADER, array('UNIONBANK-UBIZ-APP-ID:              '.$api_id.'', 
			//                                            'UNIONBANK-UBIZ-APP-SIGNATURE:       '.$api_sig.'', 
			//                                            'UNIONBANK-UBIZ-APP-MERCHANT-CODE:   '.$merchant_code.'', 
			//                                            'UNIONBANK-UBIZ-REQUEST-EXPIRES:     '.$api_expires.''));
			// curl_setopt($ch,CURLOPT_POST,count($fields));
			// curl_setopt($ch,CURLOPT_POSTFIELDS,$fields_string);
			// curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

			// if(env('UB_ENVIRONMENT') == 'PROD'){
			//  //IMPORTANT: UNCOMMENT for PROD environment
			//  //Note that even if your data is sent over SSL secured site, a certificate checking is recommended as an added security,
			//  //by ensuring that your CURL session will not just trust any server certificate
			//  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, TRUE);                       // verify peer's certificate 
			//  curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);                          // check the existence of a common name and also verify that it matches the hostname provided
			//  // curl_setopt($ch, CURLOPT_CAINFO, getcwd() . "/path/to/file.crt");     // name of a file holding one or more certificates to verify the peer with. 
			//                                                                          // see documentation #before-going-live section for notes regarding certificate file.
			// }

			// $result = curl_exec($ch);   

			// if(curl_errno($ch))
			// {
			//     //you can log the curl error
			//     $message = curl_errno($ch).': '.curl_error($ch);
			// }
			// else
			// {
			//     //you can log the post information
			//     $info = curl_getinfo($ch);
			//     $message = 'POST '. $info['url'].' in '.$info['total_time'].' secs. ' . $info['http_code'] .' - '.$fields_string;

			//     //process returned JSON response, e.g save response
			//     $data = json_decode($result);

			//     if($data->meta->response_code == '200')
			//     {
			//         //redirect your customer to UBIZ payment page
			//      return response()->json([
			//      	'payment_method' => 'ushare',
			//      	'checkout_url' => $data->response->checkout_url
			//      ]);
			//     }
			//     else
			//     {
			//         //capture error and do necessary process
			//         //log error details 
			//         echo 'Log error details: '.$data->meta->error_type.' | '.$data->meta->error_detail.' | '.$data->meta->error_code;

			//         //to force error for testing purposes you can change expires value: $api_expires = $gmt_now - 28800 + 10800;
			//     }
			// }    

			// echo '<hr />';
			// echo "CURL action: <pre>$message;</pre>";
			// echo "CURL raw result: <pre>".print_r($result, true)."</pre>";     

			// curl_close($ch);

			/* End of checkouts API call - PHP CODE GUIDE */
		} else {
			return response()->json([
				'payment_method' => 'bpi'
			]);
		}
	}


	public function admin_proceed_registration(Request $request, $event_id)
	{
		$user_id = Auth::user()->role == 3 ? $request->user_id : Auth::id();
		$event = DB::table('pspevents')->where('id', $event_id)->select('id', 'title', 'end_registration')->first();
		$end_dt = \Carbon\Carbon::parse(date($event->end_registration));
		$end_dt = \Carbon\Carbon::parse(date($end_dt));
		if (Auth::user()->role == 3) {
			$payment_method = $request->payment_method;
			$rate_id = $request->rate_id;
			$accommodation_id = $request->accommodation_id;
			$total = $request->total;
			$imageName = $request->file == null ? '' : 'event_' . $event_id . '-user_' . $user_id . '.' . $request->file->getClientOriginalExtension();
			if ($request->file != null) {

				$request->file->move(public_path('uploads/sc_id'), $imageName);
			}
			$payment_details = DB::table('payment_details')
				->insert([
					'pspevent_id' => $event_id,
					'user_id' => $user_id,
					'rate_id' => $rate_id,
					'accommodation_id' => $accommodation_id  != 'null' ? $accommodation_id : 0,
					'sc_file_path' => $request->file  != null ? '/uploads/sc_id/' . 'event_' . $event_id . '-user_' . $user_id . '.' . $request->file->getClientOriginalExtension() : '',
					'payment_method' => $payment_method,
					'discount' => $request->discount,
					'discount_type' => $request->discount_type,
					'discount_percent' => $request->discount_percent,
					'total' => number_format((float)$request->total, 2, '.', ''),
					'additional_fee_id' => $request->additional_fee_id,
					'created_at' => \Carbon\Carbon::now(),
					'updated_at' => \Carbon\Carbon::now(),
				]);

			if ($accommodation_id != '0') {
				$accommodation = Accommodation::find($accommodation_id);
				if ($accommodation->isLimited == 1) {
					$accommodation->limit = $accommodation->limit - 1;
					$accommodation->save();
				}
			}
		} else {

			$payment_method = $request->payment_method;
			$rate_id = $request->rate_id;
			$accommodation_id = $request->accommodation_id;
			$total = $request->total;
			$imageName = $request->file == null ? '' : 'event_' . $event_id . '-user_' . $user_id . '.' . $request->file->getClientOriginalExtension();
			if ($request->file != null) {

				$request->file->move(public_path('uploads/sc_id'), $imageName);
			}
			$payment_details = DB::table('payment_details')
				->insert([
					'pspevent_id' => $event_id,
					'user_id' => $user_id,
					'rate_id' => $rate_id,
					'accommodation_id' => $accommodation_id  != 'null' ? $accommodation_id : 0,
					'sc_file_path' => $request->file  != null ? '/uploads/sc_id/' . 'event_' . $event_id . '-user_' . $user_id . '.' . $request->file->getClientOriginalExtension() : '',
					'payment_method' => $payment_method,
					'discount' => $request->discount,
					'discount_type' => $request->discount_type,
					'discount_percent' => $request->discount_percent,
					'total' => number_format((float)$request->total, 2, '.', ''),
					'additional_fee_id' => $request->additional_fee_id,
					'created_at' => \Carbon\Carbon::now(),
					'updated_at' => \Carbon\Carbon::now(),
				]);
			$member = \App\User::find(Auth::id());
			if ($request->total > 0) {

				self::emailMemberRegistrationToEvent($member, $event, $request->root());
			}
			if ($accommodation_id != '0') {
				$accommodation = Accommodation::find($accommodation_id);
				if ($accommodation->isLimited == 1) {
					$accommodation->limit = $accommodation->limit - 1;
					$accommodation->save();
				}
			}
		}


		if ($payment_method == 'ushare') {
			// ushare api
		} else {
			return response()->json([
				'payment_method' => 'bpi'
			]);
		}
	}


	public function upload_receipt(Request $request, $event_id)
	{
		$imageName = 'event_' . $event_id . '-user_' . Auth::id() . '.' . $request->file->getClientOriginalExtension();
		$request->file->move(public_path('uploads/bpi-receipt'), $imageName);
		$payment_details = DB::table('payment_details')
			->where('pspevent_id', $event_id)
			->where('user_id', Auth::id())
			->update([
				'bpi_file_path' => '/uploads/bpi-receipt/' . 'event_' . $event_id . '-user_' . Auth::id() . '.' . $request->file->getClientOriginalExtension()
			]);
		return response()->json([
			'status' => 'success',
		]);
	}

	public function show($event_id, Request $request)
	{
		$event = DB::table('pspevents')->where('id', $event_id)->first();
		$rates = DB::table('rates')->where('pspevent_id', $event_id)->get();
		$pagePerItem = 500;
		$event->participants = payment_details::with('member_details')->with('rate_details')->where('pspevent_id', $event_id)->where('is_accepted', $request->sort ? $request->sort : 0)->paginate($pagePerItem);
		if ($request->sort)
			foreach ($event->participants as $key => $value) {
				$json = '{"event_id": ' . $event->id . ', "user_id": ' . $value->user_id . ', "first_name": "' . $value->member_details->first_name . '"'  . ', "last_name": "' . $value->member_details->last_name . '"}';
				$event->participants[$key]['qr_code'] = $json;
			}
		// dd($event);

		return view('participants.show', ['event' => $event, 'rates' => json_encode($rates), 'eventProps' => json_encode($event)]);
	}

	public function acceptMember(Request $request, $event_id, $member_id)
	{
		$member = DB::table('users')->where('id', $member_id)->first();
		$event = DB::table('pspevents')->where('id', $event_id)->first();
		$payment_details = payment_details::where('pspevent_id', $event_id)->where('user_id', $member_id)->first();
		// dd($payment_details->payment_method == 'bpi');
		if ($payment_details->payment_method == 'bpi') {
			if ($payment_details->discount_percent == '100') {
				$payment_details->is_accepted = 1;
				// $payment_details->qrcode = self::generateQrCode($member, $payment_details->id);
				// $image = \QrCode::format('png')
				//                  ->size(400)->errorCorrection('H')
				//                  ->generate($payment_details->qrcode);
				// $output_file = '/public/' . $payment_details->id . '.png';
				// Storage::disk('local')->put($output_file, $image);                	
				$payment_details->save();
				self::emailAcceptMember($member, $event, $payment_details->id, $request->root());
				return redirect('/events/' . $event_id . '/participants');
			} else {
				if ($payment_details->bpi_file_path != null) {
					$payment_details->is_accepted = 1;
					// $payment_details->qrcode = self::generateQrCode($member, $payment_details->id);
					// $image = \QrCode::format('png')
					//                  ->size(400)->errorCorrection('H')
					//                  ->generate($payment_details->qrcode);
					// $output_file = '/public/' . $payment_details->id . '.png';
					// Storage::disk('local')->put($output_file, $image);                	
					$payment_details->save();
					self::emailAcceptMember($member, $event, $payment_details->id, $request->root());
					return redirect('/events/' . $event_id . '/participants');
				} else {
					return 'Process invalid';
				}
			}
		} else {
			$payment_details->is_accepted = 1;
			// $payment_details->qrcode = self::generateQrCode($member, $payment_details->id);
			// $image = \QrCode::format('png')
			// 	                 ->size(400)->errorCorrection('H')
			// 	                 ->generate($payment_details->qrcode);
			// 	$output_file = '/public/' . $payment_details->id . '.png';
			// 	Storage::disk('local')->put($output_file, $image);                	
			$payment_details->save();
			self::emailAcceptMember($member, $event, $payment_details->id, $request->root());
			return redirect('/events/' . $event_id . '/participants');
		}
		// if($payment_details->is_accepted == 0){
		// }else{
		// 	return 'Already accepted.';
		// }
	}

	private function generateQrCode($member, $payment_details_id)
	{
		$fullname = $member->first_name . " " . $member->last_name;
		if ($member->classification == "Diplomate") {
			return 'D' . str_pad($payment_details_id, 4, '0', STR_PAD_LEFT) . "_" . $fullname;
		} else if ($member->classification == "Junior") {
			return 'J' . str_pad($payment_details_id, 4, '0', STR_PAD_LEFT) . "_" . $fullname;
		} else if ($member->classification == "Life Member") {
			return 'L' . str_pad($payment_details_id, 4, '0', STR_PAD_LEFT) . "_" . $fullname;
		} else if ($member->classification == "Fellow") {
			return 'F' . str_pad($payment_details_id, 4, '0', STR_PAD_LEFT) . "_" . $fullname;
		} else {
			return 'G' . str_pad($payment_details_id, 4, '0', STR_PAD_LEFT) . "_" . $fullname;
		}
	}

	private function emailAcceptMember($member, $event, $pd_id, $host)
	{
		$to_email = $member->email;
		$data = array("name" => $member->first_name, 'event' => $event, 'pd_id' => $pd_id, 'host' => $host);
		Mail::send('emails.acceptmembermail', $data, function ($message) use ($to_email) {
			$message->to($to_email)
				->subject('PSP Events Payment Confirmation');
			$message->from('pspeventsman@gmail.com', 'PSP Events Payment Confirmation');
		});
	}

	private function emailMemberRegistrationToEvent($member, $event, $host)
	{
		$to_email = $member->email;
		$data = array("name" => $member->first_name, 'event' => $event, 'host' => $host);
		Mail::send('emails.registermembermail', $data, function ($message) use ($to_email) {
			$message->to($to_email)
				->subject('PSP Events Registration Submitted');
			$message->from('pspeventsman@gmail.com', 'PSP Events Registration');
		});
	}

	public function redirect()
	{
		return view('redirect');
	}

	public function deleteRegistration(Request $request)
	{
		$payment_details = DB::table('payment_details')
			->where('id', $request->payment_details_id)
			->first();
		$toArray = (array) $payment_details;
		$delete_registration = DB::table('registration_delete_details')
			->insert($toArray);
		$pd_delete =  $payment_details = DB::table('payment_details')
			->where('id', $request->payment_details_id)
			->delete();

		return response()->json(['status' => 'ok']);
	}

	public function updateTransactionNumber(Request $request)
	{
		$payment_details = DB::table('payment_details')
			->where('id', $request->payment_details_id)
			->update([
				'transaction_number' => $request->transaction_number
			]);
		return response()->json(['transaction_number' => $request->transaction_number]);
	}
	public function updateTransactionDate(Request $request)
	{
		$payment_details = DB::table('payment_details')
			->where('id', $request->payment_details_id)
			->update([
				'transaction_date' => $request->transaction_date
			]);
		return response()->json(['transaction_date' => $request->transaction_date]);
	}
	public function sort($event_id, Request $request)
	{
		$event = DB::table('pspevents')->where('id', $event_id)->first();
		$event->participants = payment_details::with('member_details')->with('rate_details')->where('pspevent_id', $event_id)->where('is_accepted', $request->status)->get();
		$rates = DB::table('rates')->where('pspevent_id', $event_id)->get();
		return response()->json(['event' =>  $event, 'rates' => json_encode($rates)]);
	}

	public function export()
	{
		return Excel::download(new UsersExport(), 'registrants.xlsx');
		// return \App\User::with('payment_detail.rate_details', 'payment_detail.additional_fee_details')->withCount('payment_details')->having('payment_details_count', '>', 0)->get();
	}

	public function export_participants($event_id)
	{
		return Excel::download(new PaymentExport($event_id), 'registrants.xlsx');
		return $payment->member_details;
	}
}
