<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\User;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Mail;

class MemberController extends Controller
{
    public function index()
    {
        return view('members.index');
    }



    function paginate()
    {
        $data = DB::table('users')->where('role', 2)->orderBy('id', 'desc')->paginate(10);
        return response()->json($data);
    }

    function update(Request $request)
    {

        $member = DB::table('users')
            ->where('id', $request->member_id)
            ->update([
                'first_name' => $request->first_name,
                'last_name' => $request->last_name,
                'classification' => $request->classification != 'Non Member' ? $request->classification : null,
                'contact' => $request->contact,
                'email' => $request->email,
                'affiliation' => $request->affiliation,
            ]);
        return response()->json(['status' => 'ok']);
    }

    public function verify(Request $request, $id)
    {
        $member = User::find($id);
        $verification_cd = Str::random(32);
        // $member->email_verified_at = \Carbon\Carbon::now();
        // $member->created_at = \Carbon\Carbon::now();
        // $member->is_active = 1;
        // $member->save();
        // $this->sendVerificationEmail($member, $request->root());
        // return response()->json(['status' => 'ok']);        
        if (!DB::table('users')->where('verification_cd', $verification_cd)->exists()) {
            $verification_cd = Str::random(32);
            $member->verification_cd = $verification_cd;
            $member->email_verified_at = \Carbon\Carbon::now();
            $member->created_at = \Carbon\Carbon::now();
            $member->is_active = 1;
            $this->sendVerificationEmail($member, $request->root());
            $member->save();
            return response()->json(['status' => 'ok']);
        } else {
            return response()->json(['status' => 'failed']);
        }
    }

    private function sendVerificationEmail($member, $host)
    {
        $to_email = $member->email;
        $data = array("member" => $member, 'host' => $host, 'username' => $member->username, 'password' => $member->reset_password_key);
        Mail::send('emails.mail', $data, function ($message) use ($to_email) {
            $message->to($to_email)
                ->subject('PSP Events Verification');
            $message->from('pspeventsman@gmail.com', 'PSP Events Verification');
        });
    }

    public function verifysuccess($verification_cd)
    {
        $member = DB::table('users')->where('verification_cd', $verification_cd)->update(['is_active' => 1, 'verification_cd' => null]);
        return redirect('/login');
    }

    public function search(Request $request)
    {
        $members = DB::table('users')->where('role', 2)->where('first_name', 'like', '%' . $request->search_string . '%')->orWhere('last_name', 'like', '%' . $request->search_string . '%')->get();
        return response()->json(['status' => 'ok', 'members' => $members]);
    }

    public function signInAs(Request $request)
    {
        Auth::loginUsingId($request->participant_id);
        // return '/events/'.$request->event_id.'/';
        return redirect(\route('showEvent', $request->event_id));
    }
}
