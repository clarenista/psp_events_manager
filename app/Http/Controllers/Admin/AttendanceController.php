<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Attendance;
use Carbon\Carbon;

class AttendanceController extends Controller
{
    function index($event_id){
        $attendance = Attendance::with('user')->groupBy('user_id')->orderBy('created_at', 'desc');
        $from = Carbon::parse(\request()->date_from);
        $to = Carbon::parse(\request()->date_to)->addHours(23)->addMinutes(59);
        // dd(\request()->date_from);
        // dd($attendance->whereBetween('created_at', [\request()->date_from, \request()->date_to])->get());
        // dd($attendance->whereBetween('created_at', [\request()->date_from, \request()->date_to])->get());
        if(\request()->has(['date_from', 'date_to'])){
            $attendance->whereBetween('created_at', [$from, $to]);
        }
        // dd($attendance->where('type', \request()->type)->get());
        if(\request()->has('type')){
            $attendance->where('type', \request()->type);
        }
        $fellows = 0;
        $diplomates = 0;
        $life_member = 0;
        $attendance = $attendance->get();
        
        foreach ($attendance as $a) {
            if($a->user->classification === 'Fellow') 
                $fellows++;
            elseif($a->user->classification === 'Diplomate') 
                $diplomates++;
            if($a->user->classification === 'Life Member') 
                $life_member++;
        }
        // dd($diplomate);
        return view('admin.attendance.index', compact('attendance', 'diplomates', 'fellows', 'life_member', 'event_id'));
    }
}
