<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    public function login(Request $request){
    	return view('auth.login', ['events' => json_encode(DB::table('pspevents')->orderBy('created_at', 'desc')->take(1)->get())]);
    }

    public function authenticate(Request $request){
        if (Auth::attempt(['username' => $request->username, 'password' => $request->password, 'is_active' => 1])) {
            return response()->json(['status' => 'ok','role' => Auth::user()->role]);
        }else{
            return response()->json(['status' => 'failed']);
        }
    }

    public function register(){
        return view('auth.register');
    }

    public function saveMember(Request $request){
        $result = DB::table('users')
                    ->where('email', $request->email)
                    ->orWhere('username', $request->username)
                    ->orWhere('first_name', $request->first_name)->where('last_name', $request->last_name)->where('affiliation', $request->affiliation)
                    ->get();
        if(count($result) > 0){
            return response()->json(['status' => 'exists', 'users' => $result]);
        }
        $classification = $request->classification != '' ? $request->classification : null;
        $member = DB::table('users')
                    ->insert([
                        'first_name' => $request->first_name,
                        'last_name' => $request->last_name,
                        'contact' => $request->contact,
                        'affiliation' => $request->affiliation,
                        'email' => $request->email,
                        'username' => $request->username,
                        'is_active' => 0,
                        'role' => 2,
                        'password' => Hash::make($request->password),
                        // 'reset_password_key ' => $request->password,
                        'classification' => $classification
                    ]);
        self::emailSignupSuccess($request->email);
        $member_id = DB::getPdo()->lastInsertId();
        Auth::loginUsingId($member_id);
        return response()->json(['status' => 'ok']);

    }

    private function existsCheck($request){
        // dd($request->all());
        $result = DB::table('users')->where('email', $request->email)->where('first_name', 'like' , '%'.$request->first_name.'%')
                    // ->orWhere('email', $request->email)->where('username', $request->username)
                    // ->orWhere('first_name', $request->first_name)->where('last_name', $request->last_name)->where('contact', $request->contact)->where('affiliation', $request->affiliation)->where('username', $request->username)
                    ->get();
        return $result;
        // $emailExists = DB::table('users')->where('email', $request->email)->exists();
        // $usernameExists = DB::table('users')->where('username', $request->username)->exists();
        // $password = Str::random(4); 


        // if($emailExists){
        //     return response()->json(['status' => 'failed','key' => 'email', 'message' =>"Email already exists."]);
        // }
        // if($usernameExists){
        //     return response()->json(['status' => 'failed','key' => 'username', 'message' =>"username already exists."]);
        // }

    }

    private function emailSignupSuccess($email){
        $to_email = $email;
        $data = array('emai' => $email);
        Mail::send('emails.signupmail', $data, function($message) use ($to_email) {
            $message->to($to_email)
                    ->subject('PSP Events Sign-up Complete - (for admin approval)');
            $message->from('pspeventsman@gmail.com','PSP Events Sign-up Complete - (for admin approval)');
        });         
    }

    public function passwordRemind(){
        return view('auth.passwordremind');
    }

    public function sendResetPassword(Request $request){
        $email = $request->email;
        $member = User::where('email', $email)->first();
        $reset_password_key = Str::random(32);
        if($member){
            $member->reset_password_key = $reset_password_key;
            $member->save();
            self::emailResetPassword($member, \request()->root());
            return response()->json(['status' => 'success']);
        }else{
            return response()->json(['status' => 'failed']);

        }

    }

    private function emailResetPassword($member, $host){
        $to_email = $member->email;
        $data = array("member" => $member, 'reset_password_key' => $member->reset_password_key, 'host' => $host, 'username'=>$member->username);
        Mail::send('emails.pwresetmail', $data, function($message) use ($to_email) {
            $message->to($to_email)
                    ->subject('PSP Events Reset Password');
            $message->from('pspeventsman@gmail.com','PSP Events Reset Password');
        });            
    }

    function resetPassword($reset_password_key){
        $member = User::where('reset_password_key', $reset_password_key)->first();
        if($member){
            return view('auth.resetpassword', ['reset_password_key' => json_encode($reset_password_key)]);
        }else{
            return abort(404);
        }
    }

    function saveNewPassword(Request $request, $reset_password_key){
        $newPassword = $request->password;
        $member = User::where('reset_password_key', $reset_password_key)->first();
        $member->password = Hash::make($newPassword);
        $member->reset_password_key = '';
        $member->save();
        return response()->json([
            'status' => 'success'
        ]);

    }

    function logout(){
        Auth::logout();
        return redirect('/login');
    }
}
