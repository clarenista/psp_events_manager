<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;

class ProfileController extends Controller
{

    function index(){
        return view('profile.index', ['user' => Auth::user()]);
    }

    function showPassword(){
    	return view('profile.changepassword');
    }

    function updatePassword(Request $request){
    	$updatePassword = DB::table('users')
    						->where('id', Auth::id())
    						->update([
    							'password' => Hash::make($request->new_password)
    						]);
    	return response()->json(['status' => 'ok']);
    }

    function updateProfile(Request $request){
        $user = DB::table('users')
                    ->where('id', Auth::id())
                    ->update([
                        'first_name' => $request->first_name,
                        'last_name' => $request->last_name,
                        'contact' => $request->contact,
                        'affiliation' => $request->affiliation,
                    ]);
        return response()->json(['status' => 'ok']);

        

    }
}
