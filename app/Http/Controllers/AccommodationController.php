<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AccommodationController extends Controller
{

    function store(Request $request, $event_id){
    	$accommodation = DB::table('accommodations')
    			->insert([
    				'pspevent_id' => $event_id,
    				'amount' => $request->amount,
    				'category' => $request->category,
    				'isLimited' => $request->isLimited,
    				'limit' => $request->limit,
    				'created_at' => \Carbon\Carbon::now(),
    				'updated_at' => \Carbon\Carbon::now(),
    			]);
    	$id = DB::getPdo()->lastInsertId();

    	return response()->json(['status'=>'success', 'id' => $id]);
    }


    function show($event_id, $accommodation_id){
    	$accommodation_details = DB::table('accommodations')
    						->where('id', $accommodation_id)->first();
		$accommodation_details->event = DB::table('pspevents')->where('id', $event_id)->select('id')->first();    						
    	return view('accommodations.show', ['accommodation_details' => json_encode($accommodation_details)]);
    }

    function update(Request $request, $event_id, $accommodation_id){
    	$accommodation = DB::table('accommodations')
    			->where('id', $accommodation_id)
    			->update([
    				'amount' => $request->amount,
    				'category' => $request->category,
    				'isLimited' => $request->isLimited,
    				'limit' => $request->limit,
    				'updated_at' => \Carbon\Carbon::now(),					
    			]);
    	return response()->json(['status' => 'success']);
    }
}
