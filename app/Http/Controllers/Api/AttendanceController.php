<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use \App\pspevent as PspEvent;
use \App\payment_details as PaymentDetail;
use \App\User;

class AttendanceController extends Controller
{
    public function store(Request $request){
        $fields = $request->all();
        // dd($fields);
        if(!$fields) return response('Bad request', 400);
        $paymentDetails = PaymentDetail::where('pspevent_id', $fields['event_id'])->where('user_id', $fields['user_id'])->first();
        $isPaymentConfirmed = self::checkPaymentConfirmation($paymentDetails);
        $fullname = $fields['first_name']." ".$fields['last_name'];
        if($isPaymentConfirmed){
            $createdAttendance = self::createByClassification($fields);
            if($createdAttendance) return response(['status' =>'ok', 'message' => 'Welcome '.$fullname. '. Your attendance has been recorded.'], 201);
            // return response($fields);
            return response(['status' =>'failed', 'message' => $fullname. ' is not allowed to attend the business meeting.'], 400);
        }   
        return response(['status' =>'failed', 'message' => 'Bad request'], 400);

    }

    private function createByClassification($fields){
        $allowedClassification = ['Diplomate', 'Fellow', 'Life Member'];
        $user = self::isValidUser($fields);
        // dd(in_array($user->classification, $allowedClassification));
        switch ($fields['type']) {
            case 'BM':
                if(in_array($user->classification, $allowedClassification)) return self::createAttendance($fields);
                return null;
            case 'Day':
                return self::createAttendance($fields);
            
            default:
                return null;

        }

    }   

    private function checkPaymentConfirmation(PaymentDetail $paymentDetails){
        if($paymentDetails->is_accepted) return true;
        return false;
    }

    private function createAttendance($fields){
        $isValid = self::isValidUser($fields);

        if($isValid){

            $attendance = \App\Attendance::create(['pspevent_id' => $fields['event_id'],
            'user_id' => $fields['user_id'],
            'first_name' =>  $fields['first_name'],
            'last_name' => $fields['last_name'],
            'type' => $fields['type']]);
    
            return $attendance;
        }
        return null;
    }

    private function isValidUser($fields){
        $user = User::where('first_name', $fields['first_name'])->where('last_name', $fields['last_name'])->first();
        if($user) return $user;
        return null;
    }

}
