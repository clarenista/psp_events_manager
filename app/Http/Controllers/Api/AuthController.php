<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;

class AuthController extends Controller
{
    public function user(Request $request)
    {
        $user = User::with('payment_details')->where('username', $request->email)->first();
        $user2 = User::with('payment_details')->where('username', $request->email)->where('login_code', $request->password)->first();
        if ($user) {
            $password = Hash::check($request->password, $user->password);
            if ($password || $user2) {
                if (!$user) {
                    $user = $user2;
                }
                if ($user->payment_details->count() > 0) {

                    return response()->json(
                        [
                            'status' => 'ok',
                            'user' => $user,
                        ]
                    );
                } else {

                    return response()->json(
                        [
                            'status' => 'failed',
                            'message' => 'Your payment is not yet verified. Please coordinate with the PSP Secretariat.'
                        ]
                    );
                }
            } else {
                return response()->json(
                    [
                        'status' => 'failed',
                        'message' => 'Invalid credentials.'
                    ]
                );
            }
        } else {
            return response()->json(
                [
                    'status' => 'failed',
                    'message' => 'Username does not exists.'
                ]
            );
        }
    }

    public function portalLogin(Request $request)
    {
        $user = User::with('api_payment_details')->whereIn('classification', ['Diplomate', 'Fellow', 'Junior', 'Resident', 'Life Member'])->where('username', $request->email)->first();
        if ($user) {
            $password = Hash::check($request->password, $user->password);
            if ($password) {
                return response()->json(
                    [
                        'status' => 'ok',
                        'user' => $user,
                    ]
                );
            } else {
                return response()->json(
                    [
                        'status' => 'failed',
                        'message' => 'Invalid credentials.'
                    ]
                );
            }
        } else {
            return response()->json(
                [
                    'status' => 'failed',
                    'message' => 'Username does not exists.'
                ]
            );
        }
    }

    public function users()
    {
        return response()->json([
            'users' => User::withCount('payment_details')->having('payment_details_count', '>', 0)->get()
        ]);
    }

    public function getUsers()
    {
        $users = User::has('payment_details')->get();
        return $users;
    }

    public function passwordRemind()
    {
        return view('auth.passwordremind');
    }


    public function conventionSendResetPassword(Request $request)
    {
        $email = $request->email;
        $member = User::where('email', $email)->first();
        $reset_password_key = Str::random(32);
        if ($member) {
            $member->reset_password_key = $reset_password_key;
            $member->save();
            self::conventionEmailResetPassword($member, $request->domain);
            // self::conventionEmailResetPassword($member, 'http://localhost:8000');
        }
        return response()->json(['status' => 'success']);
    }

    public function portalSendResetPassword(Request $request)
    {
        $email = $request->email;
        $member = User::where('email', $email)->first();
        $reset_password_key = Str::random(32);
        if ($member) {
            $member->reset_password_key = $reset_password_key;
            $member->save();
            self::portalEmailResetPassword($member, $request->domain);
            // self::conventionEmailResetPassword($member, 'http://localhost:8000');
        }
        return response()->json(['status' => 'success']);
    }


    public function sendResetPassword(Request $request)
    {
        $email = $request->email;
        $member = User::where('email', $email)->first();
        $reset_password_key = Str::random(32);
        if ($member) {
            $member->reset_password_key = $reset_password_key;
            $member->save();
            self::emailResetPassword($member, request()->root());
        }
        return response()->json(['status' => 'success']);
    }

    private function emailResetPassword($member, $host)
    {
        $to_email = $member->email;
        $data = array("member" => $member, 'reset_password_key' => $member->reset_password_key, 'host' => $host, 'username' => $member->username);
        Mail::send('api.emails.pwresetmail', $data, function ($message) use ($to_email) {
            $message->to($to_email)
                ->subject('PSP Events Reset Password');
            $message->from('pspeventsman@gmail.com', 'PSP Events Reset Password');
        });
    }

    private function conventionEmailResetPassword($member, $host)
    {
        $to_email = $member->email;
        $data = array("member" => $member, 'reset_password_key' => $member->reset_password_key, 'host' => $host, 'username' => $member->username);
        Mail::send('emails.convention.pwresetmail', $data, function ($message) use ($to_email) {
            $message->to($to_email)
                ->subject('PSP 71st Virtual Convention Reset Password');
            $message->from('pspconvention.70@gmail.com', 'PSP 71st Virtual Convention');
        });
    }

    private function portalEmailResetPassword($member, $host)
    {
        $to_email = $member->email;
        $data = array("member" => $member, 'reset_password_key' => $member->reset_password_key, 'host' => $host, 'username' => $member->username);
        Mail::send('emails.portal.pwresetmail', $data, function ($message) use ($to_email) {
            $message->to($to_email)
                ->subject('PSP Portal Reset Password');
            $message->from('pspconvention.70@gmail.com', 'PSP Portal');
        });
    }

    function resetPassword($reset_password_key)
    {
        $member = User::where('reset_password_key', $reset_password_key)->first();
        if ($member) {
            return view('auth.resetpassword', ['reset_password_key' => json_encode($reset_password_key)]);
        } else {
            return abort(404);
        }
    }

    function saveNewPassword(Request $request)
    {
        $newPassword = $request->password;
        $member = User::where('reset_password_key', $request->reset_password_key)->first();
        $member->password = Hash::make($newPassword);
        $member->reset_password_key = '';
        $member->save();
        return response()->json([
            'status' => 'success',
            'user' => $member
        ]);
    }
    public function portalUsers(Request $request)
    {
        $user = User::with('api_payment_details')->whereIn('classification', ['Diplomate', 'Fellow', 'Junior', 'Resident', 'Life Member'])->where('email', $request->email)->first();
        return response()->json(
            [
                'status' => 'ok',
                'user' => $user,
            ]
        );
    }

    function mobileLogin(){
        
        $credentials = request()->only('username', 'password');
        $user = User::where('username', $credentials['username'])->where('role', 4)->first();
        if(!$user) return response(['status' => 'failed'], 401);    
        if(Hash::check($credentials['password'], $user->password)) return response(['status' => 'ok', 'api_token' => $user->api_token], 200);
        return response(['status' => 'failed'], 401);
    }
}
