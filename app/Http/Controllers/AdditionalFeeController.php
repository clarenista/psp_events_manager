<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AdditionalFeeController extends Controller
{
    function store(Request $request, $event_id){
    	$rate = DB::table('additional_fees')
    			->insert([
    				'pspevent_id' => $event_id,
                    'currency' => $request->currency,
    				'amount' => $request->amount,
    				'classification' => $request->classification,
    				'created_at' => \Carbon\Carbon::now(),
    				'updated_at' => \Carbon\Carbon::now(),
    			]);
    	$id = DB::getPdo()->lastInsertId();

    	return response()->json(['status'=>'success', 'id' => $id]);
    }

    function show($event_id,$addtnl_id){
    	$additional_fee_details = DB::table('additional_fees')
    					->where('id', $addtnl_id)
    					->first();
   		$additional_fee_details->event = DB::table('pspevents')->where('id', $event_id)->select('id')->first();
    	return view('additional_fees.show', ['addtnl_details' => json_encode($additional_fee_details)]);
    	// return response()->json(['rate_details' => $rate_details]);

    }

	function update(Request $request, $event_id, $addtnl_id){
		$rate = DB::table('additional_fees')
				->where('id', $addtnl_id)
				->update([
					'currency' => $request->currency,
					'amount' => $request->amount,
					'classification' => $request->classification,
					'updated_at' => \Carbon\Carbon::now(),					
				]);
		return response()->json(['status' => 'success']);
	}	
}
