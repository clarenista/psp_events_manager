<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\pspevent;
use App\payment_details;
use App\Accommodation;
use App\AdditionalFee;
use App\Voucher;
use App\User;
use App\MembershipFee;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;

class PspeventController extends Controller
{
    private const ADMIN_ROLE = 1;
    private const SUPER_ADMIN_ROLE = 3;

    public $membershipFees;

    function __construct()
    {

        $this->membershipFees = MembershipFee::select('name', 'amount', 'id')->get();
    }

    // admin controllers
    public function index()
    {
        if (Auth::user()->role == self::ADMIN_ROLE || Auth::user()->role == self::SUPER_ADMIN_ROLE) {
            $events = DB::table('pspevents')->select('id', 'title')->take(6)->get();
            return view('events.index', ['events' => $events]);
        } else {
            $events = DB::table('pspevents')->select('id', 'title', 'start_dt', 'end_dt')->get();
            foreach ($events as $key => $value) {
                $events[$key]->is_purchased = DB::table('payment_details')->where('pspevent_id', $value->id)->where('user_id', Auth::id())->first();
            }
            return view('events.user_index', ['events' => json_encode($events)]);
        }
    }

    public function store(Request $request)
    {
        $event = DB::table('pspevents')
            ->insert([
                'title' => $request->title,
                'start_dt' => $request->start_dt,
                'end_dt' => $request->end_dt,
                'start_tm' => $request->start_tm,
                'end_tm' => $request->end_tm,
                'venue' => $request->venue,
                'description' => $request->description,
                'start_registration' =>  $request->start_registration,
                'end_registration' =>  $request->end_registration,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
            ]);
        $id = DB::getPdo()->lastInsertId();
        if ($request->file_description) {
            $imageName = $id . '.' . $request->file_description->getClientOriginalExtension();
            $request->file_description->move(public_path('uploads/event-description'), $imageName);
            $updateEvent = DB::table('pspevents')
                ->where('id', $id)
                ->update([
                    'uploaded_description_file' => '/uploads/event-description/' . $id . '.' . $request->file_description->getClientOriginalExtension()
                ]);
        }

        return response()->json([
            'status' => 'success',
            'id' => $id
        ]);
    }

    public function show($id)
    {
        if (Auth::user()->role === self::ADMIN_ROLE || Auth::user()->role == self::SUPER_ADMIN_ROLE) {
            $event = pspevent::with('rates')->with('additional_fees')->with('vouchers')->find($id);
            return view('events.show', ['event' => $event]);
        } else {
            // dd(\request()->upload);
            $event = pspevent::with('rates')->with('additional_fees')->find($id);
            $event->is_purchased = payment_details::with('rate_details')->where('pspevent_id', $id)->where('user_id', Auth::id())->first();
            $membership_fees_id = $event->is_purchased ? explode(',', $event->is_purchased->membership_fees_id) : [];
            // dd($membership_fees_id);
            // foreach ($membership_fees_id as $id) {

            //     $event->is_purchased['membershipFeeDetails'] = MembershipFee::find($id);
            // }
            $addFees = [];
            if ($event->is_purchased) {
                if (\request()->upload && $event->is_purchased->bpi_file_path == null) {
                    if ($event->is_purchased->discount_percent == 100) {

                        $showUpload = false;
                    } else {
                        $showUpload = true;
                    }
                } else {
                    $showUpload = false;
                }
                // dd($event->is_purchased);
                // $addFees = AdditionalFee::where('id', $event->is_purchased->additional_fee_id)->first();
                if ($event->is_purchased->additional_fee_id != null) {

                    $addFeeIds = explode(',', $event->is_purchased->additional_fee_id);
                    foreach ($addFeeIds as $id) {
                        // var_dump($id);
                        $addFees[$id] = AdditionalFee::where('id', $id)->first();
                    }
                }
            } else {
                $showUpload = false;
            }
            // dd($showUpload);
            // dd(explode(',',$event->is_purchased->additional_fee_id));

            $pre_reg = Carbon::parse(date($event->end_registration));
            $pre_reg = Carbon::parse(date($pre_reg));
            $pre_reg_start = Carbon::parse(date($event->start_registration));
            $pre_reg_start = Carbon::parse(date($pre_reg_start));
            $end_dt = Carbon::parse(date($event->end_registration));
            $end_dt = Carbon::parse(date($end_dt));
            $is_done = '';
            if ($end_dt->lessThan(Carbon::parse(date('Y-m-d')))) {
                $is_done = "yes";
            } else {
                $is_done = "no";
            }
            $pre_reg_status = '';
            if ($pre_reg_start->lessThan(Carbon::parse(date('Y-m-d H:i:s')))) {

                if (Carbon::parse(date('Y-m-d H:i:s'))->greaterThan($pre_reg)) {
                    $pre_reg_status = 'off';
                } else {
                    $pre_reg_status = 'on';
                }
            } else {
                $pre_reg_status = 'off';
            }
            // dd($pre_reg_status);
            // dd($end_dt->lessThan(Carbon::parse(date('Y-m-d'))));
            // var_dump($end_dt->lessThan(Carbon::parse(date('Y-m-d'))));
            // dd(Carbon::parse(date('Y-m-d')));
            return view('events.user_show', [
                'event' => json_encode($event),
                'pre_reg_status' => json_encode($pre_reg_status),
                'is_done' => json_encode($is_done),
                'additional_fees_details' => json_encode($addFees, true),
                'upload' => json_encode($showUpload, true)
            ]);
        }
    }

    public function update(Request $request)
    {
        $id = $request->id;
        $event = pspevent::find($id);
        $event->title =  $request->title;
        $event->start_dt =  $request->start_dt;
        $event->end_dt =  $request->end_dt;
        $event->start_tm =  $request->start_tm;
        $event->end_tm =  $request->end_tm;
        $event->venue =  $request->venue;
        $event->description =  $request->description;
        $event->start_registration =  $request->start_registration;
        $event->end_registration =  $request->end_registration;
        if ($request->file_description != null) {
            $imageName = $id . '.' . $request->file_description->getClientOriginalExtension();
            $request->file_description->move(public_path('uploads/event-description'), $imageName);
            $updateEvent = DB::table('pspevents')
                ->where('id', $id)
                ->update([
                    'uploaded_description_file' => '/uploads/event-description/' . $id . '.' . $request->file_description->getClientOriginalExtension()
                ]);
        }
        $event->save();

        return response()->json([
            'status' => 'success',
            'uploaded_description_file' => $request->file_description != null ? '/uploads/event-description/' . $id . '.' . $request->file_description->getClientOriginalExtension() : $event->uploaded_description_file
        ]);
    }

    // user controllers
    public function user_index()
    {
        $events = DB::table('pspevents')->select('id', 'title', 'start_dt', 'end_dt')->get();
        foreach ($events as $key => $value) {
            $events[$key]->is_purchased = DB::table('payment_details')->where('pspevent_id', $value->id)->where('user_id', Auth::id())->first();
        }
        return view('events.user_index', ['events' => $events]);
    }

    public function user_show($id)
    {
        $event = pspevent::with('rates')->with('accommodations')->find($id);
        // var_dump($event);
        return view('events.user_show', ['event' => $event]);
    }

    public function adminRegisterUser($event_id, $user_id)
    {
        Auth::loginUsingId($user_id);
        return redirect(route('admin-register-user', $event_id));
        $date = new \DateTime();
        $user = DB::table('users')->where('id', $user_id)->first();
        $event = pspevent::with('rates')->with('additional_fees')->find($event_id);
        $event->is_purchased = DB::table('payment_details')->where('pspevent_id', $event_id)->where('user_id', $user_id)->exists();
        $limited_accommodations = Accommodation::where('pspevent_id', $event_id)->where('isLimited', 1)->where('limit', '>', 0)->get()->toArray();
        $unlimited_accommodations = Accommodation::where('pspevent_id', $event_id)->where('isLimited', 0)->get()->toArray();
        $event->accommodations = array_merge($limited_accommodations, $unlimited_accommodations);
        $formDatas = [
            'transaction_uuid' => uniqid(),
            'signed_date_time' => gmdate("Y-m-d\TH:i:s\Z"),
            'timeStamp' => $date->getTimestamp(),
        ];
        $pre_reg = Carbon::parse(date($event->end_registration));
        $end_dt = Carbon::parse(date($event->end_dt));

        $is_done = "no";
        $pre_reg_status = 'on';
        // print_r(json_encode(count($event->accommodations)));
        return view('events.admin_register', ['event' => json_encode($event), 'user_id' => json_encode($user_id), 'pre_reg_status' => json_encode($pre_reg_status), 'is_done' => json_encode($is_done), 'formDatas' => json_encode($formDatas), 'user' => json_encode($user)]);
    }

    public function register_user($event_id)
    {
        $date = new \DateTime();
        $event = pspevent::with('rates')->with('additional_fees')->find($event_id);
        $event->is_purchased = DB::table('payment_details')->where('pspevent_id', $event_id)->where('user_id', Auth::id())->exists();
        $limited_accommodations = Accommodation::where('pspevent_id', $event_id)->where('isLimited', 1)->where('limit', '>', 0)->get()->toArray();
        $unlimited_accommodations = Accommodation::where('pspevent_id', $event_id)->where('isLimited', 0)->get()->toArray();
        $event->accommodations = array_merge($limited_accommodations, $unlimited_accommodations);
        $formDatas = [
            'transaction_uuid' => uniqid(),
            'signed_date_time' => gmdate("Y-m-d\TH:i:s\Z"),
            'timeStamp' => $date->getTimestamp(),
        ];
        $pre_reg = Carbon::parse(date($event->end_registration));
        $pre_reg = Carbon::parse(date($pre_reg));
        $pre_reg_start = Carbon::parse(date($event->start_registration));
        $pre_reg_start = Carbon::parse(date($pre_reg_start));
        $end_dt = Carbon::parse(date($event->end_registration));
        $end_dt = Carbon::parse(date($end_dt));
        $is_done = '';
        if ($end_dt->lessThan(Carbon::parse(date('Y-m-d')))) {
            $is_done = "yes";
        } else {
            $is_done = "no";
        }
        $pre_reg_status = '';
        if ($pre_reg_start->lessThan(Carbon::parse(date('Y-m-d H:i:s')))) {

            if (Carbon::parse(date('Y-m-d H:i:s'))->greaterThan($pre_reg)) {
                $pre_reg_status = 'off';
            } else {
                $pre_reg_status = 'on';
            }
        } else {
            $pre_reg_status = 'off';
        }
        // $pre_reg = Carbon::parse(date($event->end_registration));
        // $end_dt = Carbon::parse(date($event->end_dt));
        // $is_done = '';
        // if($end_dt->lessThan(Carbon::parse(date('Y-m-d')))){
        //     $is_done = "yes";
        // }else{
        //     $is_done = "no";
        // }
        // $pre_reg_status = '';
        // if(Carbon::parse(date('Y-m-d H:i:s'))->greaterThan($pre_reg)){
        //     $pre_reg_status = 'off';
        // }else{
        //     $pre_reg_status = 'on';
        // }        

        // print_r(json_encode(count($event->accommodations)));

        return view('events.user_register', ['event' => json_encode($event), 'pre_reg_status' => json_encode($pre_reg_status), 'is_done' => json_encode($is_done), 'formDatas' => json_encode($formDatas), 'membershipFees' => $this->membershipFees]);
    }
    public function admin_register_user($event_id)
    {

        $date = new \DateTime();
        $event = pspevent::with('rates')->with('additional_fees')->find($event_id);
        $event->is_purchased = DB::table('payment_details')->where('pspevent_id', $event_id)->where('user_id', Auth::id())->exists();
        $limited_accommodations = Accommodation::where('pspevent_id', $event_id)->where('isLimited', 1)->where('limit', '>', 0)->get()->toArray();
        $unlimited_accommodations = Accommodation::where('pspevent_id', $event_id)->where('isLimited', 0)->get()->toArray();
        $event->accommodations = array_merge($limited_accommodations, $unlimited_accommodations);
        $formDatas = [
            'transaction_uuid' => uniqid(),
            'signed_date_time' => gmdate("Y-m-d\TH:i:s\Z"),
            'timeStamp' => $date->getTimestamp(),
        ];
        $pre_reg = Carbon::parse(date($event->end_registration));
        $pre_reg = Carbon::parse(date($pre_reg));
        $pre_reg_start = Carbon::parse(date($event->start_registration));
        $pre_reg_start = Carbon::parse(date($pre_reg_start));
        $end_dt = Carbon::parse(date($event->end_registration));
        $end_dt = Carbon::parse(date($end_dt));
        $is_done = 'no';
        $pre_reg_status = 'on';

        // $pre_reg = Carbon::parse(date($event->end_registration));
        // $end_dt = Carbon::parse(date($event->end_dt));
        // $is_done = '';
        // if($end_dt->lessThan(Carbon::parse(date('Y-m-d')))){
        //     $is_done = "yes";
        // }else{
        //     $is_done = "no";
        // }
        // $pre_reg_status = '';
        // if(Carbon::parse(date('Y-m-d H:i:s'))->greaterThan($pre_reg)){
        //     $pre_reg_status = 'off';
        // }else{
        //     $pre_reg_status = 'on';
        // }        

        // print_r(json_encode(count($event->accommodations)));
        return view('events.user_register', ['event' => json_encode($event), 'pre_reg_status' => json_encode($pre_reg_status), 'is_done' => json_encode($is_done), 'formDatas' => json_encode($formDatas), 'membershipFees' => $this->membershipFees]);
    }


    public function search(Request $request)
    {
        $pspevents = DB::table('pspevents')->where('title', 'like', '%' . $request->search_string . '%')->get();
        return response()->json(['status' => 'ok', 'pspevents' => $pspevents]);
    }

    public function unregistered($event_id)
    {
        $data = DB::table('users')->where('role', 2)->paginate(10);

        foreach ($data as $key => $value) {
            $data[$key]->isRegistered = DB::table('payment_details')->where('pspevent_id', $event_id)->where('user_id', $value->id)->exists();
        }

        return response()->json($data);
    }

    public function searchUnregistered(Request $request, $event_id)
    {
        $members = DB::table('users')->where('role', 2)->where('first_name', 'like', '%' . $request->search_string . '%')->orWhere('last_name', 'like', '%' . $request->search_string . '%')->get();
        foreach ($members as $key => $value) {
            $members[$key]->isRegistered = DB::table('payment_details')->where('pspevent_id', $event_id)->where('user_id', $value->id)->exists();
        }
        return response()->json(['status' => 'ok', 'members' => $members]);
    }

    public function newUser($event_id)
    {
        return view('events.newUser', ['event_id' => $event_id]);
    }

    public function saveNewUser(Request $request, $event_id)
    {
        $emailExists = DB::table('users')->where('email', $request->email)->exists();
        $usernameExists = DB::table('users')->where('username', $request->username)->exists();

        if ($emailExists) {
            return response()->json(['status' => 'failed', 'key' => 'email', 'message' => "Email already exists."]);
        }
        if ($usernameExists) {
            return response()->json(['status' => 'failed', 'key' => 'username', 'message' => "username already exists."]);
        }
        $user = DB::table('users')
            ->insert([
                'first_name' => $request->first_name,
                'last_name' => $request->last_name,
                'contact' => $request->contact,
                'email' => $request->email,
                'affiliation' => $request->affiliation,
                'classification' => $request->classification,
                'is_active' => 1,
                'role' => 2,
                'username' => $request->username,
                'password' => Hash::make($request->password),
                'email_verified_at' => \Carbon\Carbon::now(),


            ]);
        $user_id = DB::getPdo()->lastInsertId();
        return response()->json(['status' => 'ok', 'user_id' => $user_id]);
    }

    function emailRegistrants()
    {
        $registrants = payment_details::with('member_details')->where('pspevent_id', 2)->where('is_accepted', 1)->where('is_emailed', '0')->get();
        // dd($registrants);
        foreach ($registrants as $key => $value) {

            $registrant = payment_details::find($value->id);
            if ($registrant->is_emailed == 0) {
                // email registrant
                $to_email = $registrant->member_details->email;
                $files = [asset('documents/A-Guide-to-attending-the-PSP-Midyear.docx'), asset('documents/Final-Announcement-PSP-Midyear-Convention.docx')];
                $data = array("registrant" => $registrant->member_details,);
                Mail::send('emails.mailRegistrant', $data, function ($message) use ($to_email, $files) {
                    $message->to($to_email)
                        ->subject('The 70TH (PLATINUM) ANNIVERSARY MIDYEAR CONVENTION');
                    $message->from('pspinfotech19@gmail.com', 'The 70TH (PLATINUM) ANNIVERSARY MIDYEAR CONVENTION');
                    foreach ($files as $file) {
                        $message->attach($file);
                    }
                });
                $registrant->is_emailed = 1;
                $registrant->save();
            }
        }
    }

    function emailEvaluation()
    {
        $registrants = payment_details::whereHas('member_details', function ($q) {
            $q->where('classification', '!=', 'Non-Member');
        })->with('member_details')->where('pspevent_id', 2)->where('is_accepted', 1)->get();
        foreach ($registrants as $key => $value) {

            $registrant = payment_details::find($value->id);
            if ($registrant->is_evaluation_emailed == 0) {
                // email registrant
                $to_email = $registrant->member_details->email;
                $data = array("registrant" => $registrant->member_details,);
                Mail::send('emails.mailRegistrantEvaluation', $data, function ($message) use ($to_email) {
                    $message->to($to_email)
                        ->subject('The 70TH (PLATINUM) ANNIVERSARY MIDYEAR CONVENTION EVALUATION');
                    $message->from('pspconvention.70@gmail.com', 'The 70TH (PLATINUM) ANNIVERSARY MIDYEAR CONVENTION');
                });
                $registrant->is_evaluation_emailed = 1;
                $registrant->save();
            }
        }
    }

    function emailNonMemberEvaluation()
    {
        $registrants = payment_details::whereHas('member_details', function ($q) {
            $q->whereIn('classification', ['Non-Member']);
        })->with('member_details')->where('pspevent_id', 2)->where('is_accepted', 1)->get();
        // dd($registrants);
        foreach ($registrants as $key => $value) {

            $registrant = payment_details::find($value->id);
            if ($registrant->is_evaluation_emailed == 0) {
                // email registrant
                $to_email = $registrant->member_details->email;
                $data = array("registrant" => $registrant->member_details,);
                Mail::send('emails.mailNonMemberRegistrantEvaluation', $data, function ($message) use ($to_email) {
                    $message->to($to_email)
                        ->subject('The 70TH (PLATINUM) ANNIVERSARY MIDYEAR CONVENTION EVALUATION');
                    $message->from('pspconvention.70@gmail.com', 'The 70TH (PLATINUM) ANNIVERSARY MIDYEAR CONVENTION');
                });
                $registrant->is_evaluation_emailed = 1;
                $registrant->save();
            }
        }
    }
}
