<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use App\payment_details;
use App\pspevent;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'reset_password_key',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function payment_details()
    {
        return $this->hasMany(payment_details::class, 'user_id', 'id')->where('is_accepted', 1)->where('pspevent_id', 3)->latest();
    }

    public function payment_detail()
    {
        return $this->hasOne(payment_details::class, 'user_id', 'id');
    }

    public function events()
    {
        return $this->hasMany(pspevents::class, 'user_id', 'id');
    }

    public function api_payment_details()
    {
        return $this->hasMany(payment_details::class, 'user_id', 'id')->where('is_accepted', 1);
    }
}
