<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Attendance extends Model
{
    protected $fillable = ['pspevent_id',
    'user_id',
    'first_name',
    'last_name',
    'type'
    ];

    function user(){
        return $this->belongsTo('App\User', 'user_id', 'id');
    }
}
