@include('security')

<form id="payment_confirmation" action="https://testsecureacceptance.cybersource.com/pay" method="post"/>
@csrf
<?php
    foreach($_REQUEST as $name => $value) {
        $params[$name] = $value;
    }
?>
<fieldset id="confirmation">
    <h4>Redirecting, please wait...</h4>
    <p>If you are not redirected then please click the button below.</p>
    <hr>
	<input type="submit" style="display: none;" id="submit" value="Redirect now"/>
</fieldset>
    <?php
        foreach($params as $name => $value) {
            echo "<input type=\"hidden\" id=\"" . $name . "\" name=\"" . $name . "\" value=\"" . $value . "\"/>\n";
        }
        echo "<input type=\"hidden\" id=\"signature\" name=\"signature\" value=\"" . sign($params) . "\"/>\n";
    ?>
</form>
<script
  src="https://code.jquery.com/jquery-3.4.1.min.js"
  integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
  crossorigin="anonymous"></script>
<script>
	$(document).ready(function(){
		var data =  {
			'rate_id' :  $('#rate_id').val(),
			'accommodation_id' :  $('#accommodation_id').val(),
			'payment_method' :  'ushare',
			'discount_type' : $('#discount_type').val(),
			'discount_percent' : $('#discount_percent').val(),
			'total' : $('#total').val(),
			'discount' : $('#discount').val(),
		}
		// var fd = new FormData();
		// fd.append('rate_id', $('#rate_id').val())
		// fd.append('accommodation_id', $('#accommodation_id').val())
		// fd.append('payment_method', 'ushare')		
		$.post('/events/'+$('#pspevent_id').val()+'/register', data, function(){
			$('#submit').click();
			setTimeout(function(){
				// alert('test')
				$('#submit').show();
			},5000)			
			// alert('success')
		});
		// setTimeout(function(){
		// 	// alert('test')
		// 	$('#submit').click(function(){

				
		// 	})
		// },50000)
	})
</script>