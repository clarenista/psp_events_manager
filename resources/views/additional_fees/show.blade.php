@extends('layouts.app')

@section('title', 'Additional Fee')

@section('content')
	<auth-show-addtnl :props_addtnl_details="{{$addtnl_details}}"></auth-show-addtnl>
@endsection