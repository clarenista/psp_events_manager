<h3>Success!</h3>

<p>You have registered to {{$event->title}}. Please pay the corresponding fee/s and upload the transaction receipt.</p>
<p>Thank you.</p>