<p>
    Thank you for signing-up to PSP Events. The admin will approve your registration and will email the confirmation. This usually takes 4 to 24 hours. Please send a text to +63-917-166-4611 , +63-922-851-7379 or call 8920-3192  if you don't receive a confirmation email.
</p>
<p>
    Note: This is not yet your registration to the THE 70TH (PLATINUM) ANNIVERSARY MIDYEAR CONVENTION. You can only register when your SIGN UP is verified.
</p>
<br>


<p>Thank you.</p>
