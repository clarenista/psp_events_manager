Dear Dr. <strong>{{ $registrant->fname }} {{ $registrant->lname }}</strong>, <br>
<br>
<strong>Congratulations!</strong>
<p>You are shortlisted to the Online Molecular Pathology Course organized by the Council on Education and Research of the Philippine Society of Pathologists to start on May 18, 2020.
Please reply to this email to verfy your account and state your</p>
<p>FAMILY NAME: <b>{{$registrant->lname}}</b></p>
<p>FIRST NAME: <b>{{$registrant->fname}}</b></p>
<p>MIDDLE NAME: <b>{{$registrant->mname}}</b></p>
<p>An e-mail will follow confirming your enrollment to the learning management system together with the log in credentials and instructions on the use of the system.</p>
<p>Thank you.</p>
<p>Sincerely,</p>
<p>Team PSP Molecular Path Online Course</p>
