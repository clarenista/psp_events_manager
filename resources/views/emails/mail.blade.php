<p>Dear <strong>{{ $member->first_name }} {{ $member->last_name }}</strong>,</p> <br>

<p>You are now a verified user of PSP Events. To verify your account, click or copy to address bar the link below, <a href="{{$host}}/verification/{{$member->verification_cd}}">{{$host}}/verification/{{$member->verification_cd}}</p>


