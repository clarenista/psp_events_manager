Hi <strong>{{ $member->first_name }} {{ $member->last_name }}</strong>, <br>

Your username is <strong><u>{{ $username }}</u></strong>.<br>
To reset your password, click the link below, {{$host}}/reset-password/{{$reset_password_key}}