@extends('layouts.app')

@section('title', 'The Members')

@section('content')
	<auth-members-list></auth-members-list>
@endsection