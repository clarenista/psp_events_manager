@extends('layouts.app')

@section('title', 'Rate')

@section('content')
	<auth-show-rate :props_rate_details="{{$rate_details}}"></auth-show-rate>
@endsection