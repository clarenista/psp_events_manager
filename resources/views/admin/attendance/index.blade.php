@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-sm-12">
                <div class="card text-start">
                  <div class="card-body">
                    <h4 class="card-title">Filter</h4>
                    <form method="GET">

                        <div class="mb-3">
                            <label for="" class="form-label">Type</label>
                            <select class="form-select form-select-lg form-control" name="type" id="type" required>
                                <option value="">Select Type</option>
                                <option value="Day" @if(\request()->type === 'Day') selected @endif>Daily</option>
                                <option value="BM" @if(\request()->type === 'BM') selected @endif>Business Meeting</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="" class="form-label">Date From</label>
                            <input type="date" name="date_from" class="form-control" value='{{\request()->date_from ?? ""}}' required />
                        </div>
                        <div class="form-group">
                            <label for="" class="form-label">Date To</label>
                            <input type="date" name="date_to" class="form-control" value='{{\request()->date_to ?? ""}}' required />
                        </div>
                        @if(\request()->type)
                        <a href="{{route('admin.attendance.index', $event_id)}}" type="button" class="btn btn-secondary" data-bs-toggle="button" aria-pressed="false" autocomplete="off">Reset</a>
                        @endif
                        <button type="submit" class="btn btn-primary" data-bs-toggle="button" aria-pressed="false" autocomplete="off">Apply</button>
                      </div>
                    </form>
                </div>

            </div>
            <div class="col-md col-sm-12">
                <div class="card text-start">
                  <div class="card-body">
                    <h3 class="card-title">Members in attendance:</h3>
                    <hr>
                    @if(\request()->type === 'Day' || !\request()->has('type') )
                    
                    <div class="row lead text-uppercase">
                        <div class="col-md-3 col-sm-12">Total:</div>
                        <div class="col-md col-sm-12">{{count($attendance)}}</div>
                    </div>
                    @else
                    
                    <div class="row lead text-uppercase">
                        <div class="col-md-3 col-sm-12">Fellows:</div>
                        <div class="col-md col-sm-12">{{$fellows+$life_member}}</div>
                    </div>
                    <div class="row lead text-uppercase">
                        <div class="col-md-3 col-sm-12">Diplomates:</div>
                        <div class="col-md col-sm-12">{{$diplomates}}</div>
                    </div>
                    <hr>
                    <div class="row lead text-uppercase">
                        <div class="col-md-3 col-sm-12">Total:</div>
                        <div class="col-md col-sm-12">{{$life_member + $fellows + $diplomates}}</div>
                    </div>
                    
                    @endif
                  </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
<script>
    $(function(){
        const attendance_table = $('#attendance_table');
        attendance_table.dataTable();
    })
</script>

@endsection