@extends('layouts.app')

@section('title', 'Settings')

@section('content')
<div class="container">
    <ul class="nav nav-tabs" role="tablist">
        <li class="nav-item">
            <a class="nav-link active" data-toggle="tab" href="#mg">Membership Fee/s</a>
        </li>

    </ul>
</div>
<!-- Tab panes -->
<div class="tab-content">
    <div id="mf" class="container tab-pane active"><br>
        <a href="{{route('new_mf')}}" class="btn btn-primary" type="button" id="addNewBtn">Add new</a>
        <p></p>
        <table class="table  table-hover" id="mfTable">
            <thead class="">
                <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th>Amount</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @forelse($membershipFees as $fee)
                <tr>
                    <td>{{$fee->id}}</td>
                    <td>{{$fee->name}}</td>
                    <td>{{$fee->amount}}</td>
                    <td nowrap>
                        <form action="{{route('delete_mf', $fee->id)}}" method="POST">
                            @csrf
                            @method('DELETE')
                            <a href="{{route('edit_mf', $fee->id)}}" class="btn btn-success btn-sm" type="button">Edit</a> |
                            <button class="btn btn-danger btn-sm" type="submit">Delete</button>
                        </form>
                    </td>
                </tr>
                @empty
                <tr>
                    <td colspan="4">No data found</td>
                </tr>
                @endforelse
            </tbody>

        </table>
    </div>
</div>
@endsection