@extends('layouts.app')

@section('title', 'Settings')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-5 col-sm-12">

            <div class="card">
                <div class="card-header">
                    @if(isset($membershipFee->id))
                    Edit {{$membershipFee->name}} Membership fee

                    @else
                    Add new Membership fee
                    @endif
                </div>
                <div class="card-body">
                    <form action="{{isset($membershipFee->id) ? route('update_mf', $membershipFee->id) : route('store_mf')}}" method="POST">
                        @csrf
                        @if(isset($membershipFee->id))
                        @method('PUT')
                        @endif
                        <div class="form-group">
                            <label for="name">Name</label>
                            <input id="name" class="form-control" type="text" value='{{old("name", $membershipFee->name)}}' name="name" placeholder="Name here" required>
                        </div>
                        <div class="form-group">
                            <label for="amount">Amount</label>
                            <input id="amount" class="form-control" type="number" value='{{old("amount", $membershipFee->amount)}}' min="10" name="amount" placeholder="100" required>
                        </div>
                        <button class="btn btn-primary" type="submit">{{isset($membershipFee->id) ? 'Update' : 'Add'}}</button>
                        <a href='{{route("mf_index")}}' class="btn btn-danger">Go back to list</a>
                    </form>
                </div>

            </div>
        </div>
    </div>
</div>
@endsection