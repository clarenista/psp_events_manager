@extends('layouts.app')

@section('title', 'Participants')

@section('content')
<div class="container">
	<auth-show-participants :props_event="{{$eventProps}}" :props_rates="{{$rates}}" :props_user='{{auth()->user()}}'></auth-show-participants>
	<h1>Participants</h1>
	<p class="lead">Total of {{$event->participants->total()}}</p>

	<div class="col">
		<div class="row float-right">
			<div class="col">
				<div class="form-group">
					<label for="select_sort">Sort</label>
					<select id="select_sort" class="custom-select" name="">
						<option value="0" {{request()->sort == '0' ? 'selected' : ''}}>Pending</option>
						<option value="1" {{request()->sort == '1' ? 'selected' : ''}}>Paid</option>
					</select>
				</div>
			</div>
		</div>
	</div>
	<table class="table table-light table-responsive table-striped table-hover" id="participants_table">
		<thead>
			<tr>
				<th>#</th>
				<th>Name</th>
				<th>Classification</th>
				<th>Rate Selected</th>
				<th>Payment Status</th>
				<th>Discount</th>
				<th>Discount Type</th>
				<th>Total</th>
				@if(request()->sort)

				<th>QR Code</th>
				@endif
				<th>Action</th>
			</tr>
		</thead>
		<tbody>
			@foreach($event->participants as $participant)
			<tr>

				<td>{{$participant->member_details->id}}</td>
				<td>{{$participant->member_details->first_name}} {{$participant->member_details->last_name}}
				</td>
				<td>{{$participant->member_details->classification}}</td>
				<td>{{$participant->rate_details->category}}</td>
				<td>{{$participant->is_accepted == 1 ? 'PAID' : 'PENDING'}}</td>
				<td>{{$participant->discount_percent != "" ? $participant->discount_percent.'%' : 'N/A' }}</td>
				<td>

					@if($participant->discount_type == "sc")
					@if($participant->discount_percent == "100")
					<a target="_blank" href="{{$participant->sc_file_path}}">70 years old above</a>
					@endif
					<a target="_blank" href="{{$participant->sc_file_path}}">Senior Citizen</a>
					@elseif($participant->discount_type === "null")
					<p>N/A</p>
					@else
					<p>Discount Code</p>
					@endif
				</td>
				<td>{{$participant->total}}</td>
				@if(request()->sort)
				<td><img src="data:image/png;base64, {!! base64_encode(QrCode::color(255, 0, 0)->format('png')->size(150)->generate($participant->qr_code)); !!}" title="{{$participant->qr_code}}">
				</td>
				@endif
				<td>
					<form action=" {{route('signInAs')}}" method="post">
						@csrf
						<input type="hidden" value="{{$participant->member_details->id}}" name="participant_id" />
						<input type="hidden" value="{{$event->id}}" name="event_id" />
						<button type="submit" class="btn btn-primary btn-small">Sign-in As</button>
					</form>
					@if($participant->is_accepted == 0)
					@if($participant->payment_method == "bpi")
					@if ($participant->bpi_file_path != null)
					<a target="_blank" href="{{$participant->bpi_file_path}}" class="btn btn-info"><i class="fa fa-search"></i></a> <a href="{{\route('acceptMember', ['event_id'=> $event->id, 'member_id' => $participant->member_details->id  ])}}" class="btn btn-success"><i class="fa fa-check"></i></a>
					@else
					@if (
					$participant->discount_percent == "100" ||
					$participant->total === "0.00"
					)
					<a href="{{\route('acceptMember', ['event_id'=> $event->id, 'member_id' => $participant->member_details->id  ])}}" class="btn btn-success"><i class="fa fa-check"></i></a>
					@else
					<p>BPI receipt not found</p>
					@endif
					@endif
					@else
					<a href="{{\route('acceptMember', ['event_id'=> $event->id, 'member_id' => $participant->member_details->id  ])}}" class="btn btn-success"><i class="fa fa-check"></i></a>`
					@endif
					@else
					@if ($participant->bpi_file_path != null)
					Confirmed |<a target="_blank" href="{{$participant->bpi_file_path}}" class="btn btn-info">View receipt</a> | <a href="{{\route('acceptMember', ['event_id'=> $event->id, 'member_id' => $participant->member_details->id  ])}}" class="btn btn-success">Re-send email</a>
					@else
					Confirmed | <a href="{{\route('acceptMember', ['event_id'=> $event->id, 'member_id' => $participant->member_details->id  ])}}" class="btn btn-success">Re-send email</a>
					@endif
					@endif
				</td>
			</tr>
			@endforeach
		</tbody>
	</table>
	<div class="d-flex justify-content-center mt-3">

		{{ $event->participants->appends(request()->except('page'))->links() }}
	</div>

</div>
@endsection
@section('js')

<script>
	$(function() {
		$('#participants_table').DataTable({
			pageLength: 500,
			"info": false,
			"paging": false,
			"bFilter": false
		})
		$('#select_sort').change(function(e) {
			e.preventDefault()
			window.location.href = '/events/{{$event->id}}/participants?sort=' + e.target.value
		})
	})
</script>
@endsection