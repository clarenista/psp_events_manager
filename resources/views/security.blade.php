<?php

define ('HMAC_SHA256', 'sha256');
define ('SECRET_KEY', 'ade2223834e249eaa28e1d6043b41376a8379039f2be4b2f8ab95b5e0fce87acfd41fed6117142188530dce3614f57ad018b1c8c4c3b4c349e721ee6f9dab1e30266886a71da442dacc16fd08a83126e805ce6a6524e4405b837048bf5d9e9139fc9c2e4fc4f4b2db5e2c501f0fc2bc565aebdb58512403aa23c121943ad6a13');

function sign ($params) {
  return signData(buildDataToSign($params), SECRET_KEY);
}

function signData($data, $secretKey) {
    return base64_encode(hash_hmac('sha256', $data, $secretKey, true));
}

function buildDataToSign($params) {
        $signedFieldNames = explode(",",$params["signed_field_names"]);
        foreach ($signedFieldNames as $field) {
           $dataToSign[] = $field . "=" . $params[$field];
        }
        return commaSeparate($dataToSign);
}

function commaSeparate ($dataToSign) {
    return implode(",",$dataToSign);
}

?>
