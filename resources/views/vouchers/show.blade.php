@extends('layouts.app')

@section('title', 'Voucher')

@section('content')
	<auth-show-vouchers :props_voucher_details="{{$voucher_details}}"></auth-show-vouchers>
@endsection