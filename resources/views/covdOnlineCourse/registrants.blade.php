@extends('layouts.app')

@section('content')
	<admin-c19-pcr-registrants :props_registrants="{{$registrants}}"  :props_settings="{{$settings}}"></admin-c19-pcr-registrants>
@endsection
