
<div class="container-fluid">
  <div class="row">
    <nav class="col-md-2 d-none d-md-block bg-light sidebar">
      <div class="sidebar-sticky">
        <ul class="nav flex-column mt-5 pt-5">
          <li class="nav-item">
            <a class="nav-link active" href="/home">
              Dashboard <span class="sr-only">(current)</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="/events">
              Events
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="members">
              Members
            </a>
          </li>
        </ul>
      </div>
    </nav>


  </div>
</div>