<nav class="navbar navbar-expand-lg navbar-dark bg-primary">
	<div class="container">
		
	  <a class="navbar-brand" href="#">PSP Events</a>
	  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor03" aria-controls="navbarColor03" aria-expanded="false" aria-label="Toggle navigation">
	    <span class="navbar-toggler-icon"></span>
	  </button>

	  <div class="collapse navbar-collapse" id="navbarColor03">
	    <ul class="navbar-nav mr-auto">
	      <li class="nav-item">
	        <a class="nav-link" href="/dashboard">Dashboard <span class="sr-only">(current)</span></a>
	      </li>
	      <li class="nav-item">
	        <a class="nav-link" href="/events">Events</a>
	      </li>
	      <li class="nav-item">
	        <a class="nav-link" href="/profile">Profile</a>
	      </li>
	    </ul>
	    <form class="form-inline my-2 my-lg-0">
	      <a class="btn btn-secondary btn-lg rounded-0" href="/logout">Sign out</a>
	    </form>
	  </div>
	</div>
</nav>

