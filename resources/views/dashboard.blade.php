@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    Welcome {{Auth::user()->first_name}}
                    <hr>
                    @if(Auth::user()->role == 2)
                    <div class="row">
                        <div class="col">
                            <div class="card text-center">
                                <div class="card-body">
                                    <h4 class="card-title"><i class="fa fa-key"></i> Change my password</h4>
                                    <p class="card-text">Click the button below to redirect to change password page.</p>
                                    <a href="/profile/change-password" class="btn btn-primary">Change password now</a>
                                </div>
                            </div>
                        </div>
                        <div class="col">
                            <div class="card text-center">
                                <div class="card-body">
                                    <h4 class="card-title"><i class="fa fa-calendar"></i> The Events</h4>
                                    <p class="card-text">To view list of events, click the button below.</p>
                                    <a href="/events" class="btn btn-primary">View events</a>
                                </div>
                            </div>
                        </div>                        
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
