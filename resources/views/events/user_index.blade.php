@extends('layouts.app')

@section('title', 'Lists of Events')

@section('content')
	<members-events-component :props_events="{{$events}}"></members-events-component>
@endsection