@extends('layouts.app')

@section('title', 'Event details')

@section('content')
<members-showevent-component :props_additional_fees_details="{{$additional_fees_details}}" :props_user="{{Auth::user()}}" :props_event="{{$event}}" :props_is_done="{{$is_done}}" :props_pre_reg_status="{{$pre_reg_status}}" :props_upload="{{$upload}}"></members-showevent-component>
@endsection