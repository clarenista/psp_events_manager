@extends('layouts.app')

@section('title', 'Event New User')

@section('content')
	<admin-usersignup-component :props_eventid="{{$event_id}}"></admin-usersignup-component>
@endsection