@extends('layouts.app')

@section('title', 'The Events')

@section('content')
	<auth-events-component :events="{{$events}}"></auth-events-component>
@endsection