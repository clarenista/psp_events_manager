@extends('layouts.app')

@section('title', 'The Events')

@section('content')
	<auth-showevent-component :event="{{$event}}"></auth-showevent-component>
@endsection