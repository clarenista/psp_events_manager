@extends('layouts.app')

@section('title', 'Event Registration')

@section('content')
	<admin-eventregistration-component
		:props_event="{{$event}}"
		:props_is_done="{{$is_done}}"
		:props_pre_reg_status="{{$pre_reg_status}}"
		:props_formdatas="{{$formDatas}}"
		:props_userid="{{$user_id}}"
		:props_user="{{$user}}"
	></admin-eventregistration-component>
@endsection