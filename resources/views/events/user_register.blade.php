@extends('layouts.app')

@section('title', 'Event Registration')

@section('content')
<members-eventregistration-component :props_user="{{Auth::user()}}" :props_event="{{$event}}" :props_is_done="{{$is_done}}" :props_pre_reg_status="{{$pre_reg_status}}" :props_formdatas="{{$formDatas}}" :props_membershipfees="{{$membershipFees}}"></members-eventregistration-component>
@endsection