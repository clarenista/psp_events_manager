@extends('layouts.app')

@section('title', 'My Profile')

@section('content')
	<members-profile :props_user="{{$user}}"> </members-profile>
@endsection