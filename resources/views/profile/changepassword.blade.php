@extends('layouts.app')

@section('title', 'Change my password')

@section('content')
	<members-change-password :props_my_details="{{Auth::user()}}"></members-change-password>
@endsection