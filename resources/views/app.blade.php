<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>
        @yield('title')
    </title>


    <link rel="stylesheet" href="{{asset('css/app.css')}}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.10.2/css/all.min.css">
</head>

<body id="body">
    <div class="d-flex justify-content-center">
        <div class="col-lg-2 col-sm-12">
            <a class="navbar-brand" href="/">
                <img src="/images/pspeventslogo.png" class="img-fluid" alt="">
            </a>

        </div>
    </div>
    <hr>
    <div id="app">
        @yield('content')
    </div>
    <script src="https://superal.github.io/canvas2image/canvas2image.js"></script>
    <script src="{{asset('js/app.js')}}?v1.0.3"></script>
</body>

</html>