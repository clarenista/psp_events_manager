import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export const store = new Vuex.Store({

	state:{
		events: [],
        currencies: [
            {key: 'php', value: 'Philippine Peso'},
            {key: 'usd', value: 'US Dollar'},
        ],	
        classifications: [
            {value: 'Life Member'},
            {value: 'Fellow'},
            {value: 'Non-Member'},
            {value: 'Resident'},
            {value: 'Diplomate'},
            {value: 'Junior'},
        ],        
		yearLevels:[
        	{key: '3rd', value: '3rd year'},
        	{key: '4th', value: '4th year'},
        ],
        total: '',
        per_page: '',
        current_page: 0,
        last_page: '',
        first_page_url: '',
        last_page_url: '',
        next_page_url: '',
        prev_page_url: '',
        path: '',
        text_search : '',
        nullPrev: null,
        nullNext: null,
        isSearchList: false,    
        isLoading: true,   
        members: [],
        specialties: [
        	{key: 'ac', value: 'AP-CP'},
        	{key: 'c', value: 'CP'},
        	{key: 'ap-participation', value: 'AP (participation only)'},
        ],
        mClassifications: [
        	{key: 'F', value: 'FELLOW'},
        	{key: 'D', value: 'DIPLOMATE'},
        ],
        cMembership: [
        	{key: 'clc', value: 'Central Luzon Chapter'},
        	{key: 'nlc', value: 'North Luzon Chapter'},
        	{key: 'slc', value: 'South Luzon Chapter'},
        	{key: 'bic', value: 'Bicol Chapter'},
        	{key: 'cvc', value: 'Central Visayas Chapter'},
        	{key: 'wvc', value: 'Western Visayas Chapter'},
        	{key: 'nmc', value: 'Northern Mindanao Chapter'},
        	{key: 'smc', value: 'Southern Mindanao Chapter'},
        	{key: 'swmc', value: 'South Western Mindanao Chapter'},
        	{key: '0', value: 'None'},
        ],
        triedTakingOnlineCourseChoices: [
        	{key: 'Y', value: 'YES'},
        	{key: 'N', value: 'NO'},
        ],
        levelsOfComputerLiteracy: [
        	{key: '1', value: 'I am a computer illiterate'},
        	{key: '2', value: 'I can log in to my email or social media account like Facebook'},
        	{key: '3', value: 'I know how use the “forgot password”'},
        	{key: '4', value: 'I can compose and send email, read my inbox and delete messages'},
        	{key: '5', value: 'I can open, create word file, powerpoint, excel'},
        	{key: '6', value: 'I can browse the internet without help'},
        	{key: '7', value: 'I can save/store, open and search files from its location e.g. thumb drive'},

        ]
	},
	mutations:{
		updateEvents(state, events){
			state.events = events
		},
		addEvent(state, event){
			state.events.push(event)
		},
		updatePerPage(state, per_page){
			state.per_page = per_page
		},
		updateCurrentPage(state, current_page){
			state.current_page = current_page

		},
		updateFirstPage(state, first_page_url){
			state.first_page_url = first_page_url
		},
		updateLastPage(state, last_page){
			state.last_page = last_page
		},
		updateLastPageUrl(state, last_page_url){
			state.last_page_url = last_page_url

		},
		updateNextPageUrl(state, next_page_url){
			state.next_page_url = next_page_url

		},
		updatePath(state, path){
			state.path = path

		},
		updateTotal(state, total){
			state.total = total

		},
		updateMembers(state, members){
			state.members = members
		},
		updatePrevPageUrl(state, prev_page_url){
			state.prev_page_url = prev_page_url

		},
		updateNullPrev(state, nullPrev){
			state.nullPrev = nullPrev

		},
		updateNullNext(state, nullNext){
			state.nullNext = nullNext

		},
		addToClassification(state, classification){
			state.classifications.push({value: classification})

		}



		              //   this.$store.commit('updatePerPage', {per_page:data.per_page});
                // this.$store.commit('updateCurrentPage', {current_page:data.current_page});
                // this.$store.commit('updateFirstPage', {first_page_url:data.first_page_url});
                // this.$store.commit('updateLastPage', {last_page:data.last_page});
                // this.$store.commit('updateLastPageUrl', {last_page_url:data.last_page_url});
                // this.$store.commit('updateNextPageUrl', {next_page_url:data.next_page_url});
                // this.$store.commit('updatePath', {path:data.path});
                // this.$store.commit('updateTotal', {total:data.total});
                // this.$store.commit('updateMembers', {members:data.members});
                // this.$store.commit('updatePrevPageUrl', {prev_page_url:data.prev_page_url});
	},
	getters:{
		events: state=>state.events,
		currencies: state=>state.currencies,
		classifications: state=>state.classifications,
		total: state=>state.total,
		per_page: state=>state.per_page,
		current_page: state=>state.current_page,
		last_page: state=>state.last_page,
		first_page_url: state=>state.first_page_url,
		last_page_url: state=>state.last_page_url,
		next_page_url: state=>state.next_page_url,
		prev_page_url: state=>state.prev_page_url,
		path: state=>state.path,
		text_search: state=>state.text_search,
		members: state=>state.members,
		nullPrev: state=>state.nullPrev,
		nullNext: state=>state.nullNext,
		isSearchList: state=>state.isSearchList,
		isLoading: state=>state.isLoading,
		specialties: state=>state.specialties,
		mClassifications: state=>state.mClassifications,
		cMembership: state=>state.cMembership,
		triedTakingOnlineCourseChoices: state=>state.triedTakingOnlineCourseChoices,
		levelsOfComputerLiteracy: state=>state.levelsOfComputerLiteracy,
		yearLevels: state=>state.yearLevels,

	}
})