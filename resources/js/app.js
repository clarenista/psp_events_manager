/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

// import BootstrapVue from 'bootstrap-vue'
// import VueCtkDateTimePicker from 'vue-ctk-date-time-picker';
// import 'vue-ctk-date-time-picker/dist/vue-ctk-date-time-picker.css';
import { store } from './store/store'
// Basic Use - Covers most scenarios

// Vue.use(BootstrapVue)


// Vue.component('VueCtkDateTimePicker', VueCtkDateTimePicker);
/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))



Vue.component('form-text', require('./components/FormText.vue').default);
Vue.component('form-select', require('./components/FormSelect.vue').default);
Vue.component('slotted-card', require('./components/Card.vue').default);
Vue.component('loader', require('./components/Loader.vue').default);


// admin
Vue.component('auth-events-lists', require('./auth/EventLists.vue').default);
Vue.component('auth-events-component', require('./auth/EventComponent.vue').default);
Vue.component('auth-showevent-component', require('./auth/ShowEventComponent.vue').default);
Vue.component('auth-event-rates', require('./auth/EventRates.vue').default);
Vue.component('auth-event-accommodations', require('./auth/EventAccommodations.vue').default);
Vue.component('auth-show-rate', require('./auth/ShowRates.vue').default);
Vue.component('auth-show-accommodation', require('./auth/ShowAccommodation.vue').default);
Vue.component('auth-show-participants', require('./auth/ShowParticipants.vue').default);

Vue.component('auth-event-additional-fees', require('./auth/EventAdditionalFees.vue').default);
Vue.component('auth-show-addtnl', require('./auth/ShowAdditional.vue').default);

Vue.component('auth-event-vouchers', require('./auth/EventVouchers.vue').default);
Vue.component('auth-show-vouchers', require('./auth/ShowVouchers.vue').default);

Vue.component('auth-members-list', require('./auth/MembersList.vue').default);
Vue.component('members-list-component', require('./auth/MembersListComponent.vue').default);
Vue.component('admin-eventregistration-component', require('./auth/AdminEventRegistration.vue').default);
Vue.component('admin-usersignup-component', require('./auth/AdminUserSignUp.vue').default);

Vue.component('admin-c19-pcr-registrants', require('./auth/AdminC19Registrants.vue').default);


// public
Vue.component('public-home', require('./public/Home.vue').default);
Vue.component('public-register', require('./public/Register.vue').default);
Vue.component('public-password-remind', require('./public/PasswordRemind.vue').default);
Vue.component('public-reset-password', require('./public/ResetPassword.vue').default);

Vue.component('public-covd19-online-course', require('./public/C19OnlineCourse.vue').default);
Vue.component('public-covd19-online-course-residents', require('./public/ResidentsC19OnlineCourse.vue').default);

// members
Vue.component('members-events-component', require('./members/EventComponent.vue').default);
Vue.component('members-events-component-rates-list', require('./members/RatesList.vue').default);
Vue.component('members-showevent-component', require('./members/ShowEventComponent.vue').default);
Vue.component('default-showevent-component', require('./members/DefaultEventComponent.vue').default);
Vue.component('non-member-showevent-component', require('./members/NonMemberEventComponent.vue').default);


Vue.component('members-eventregistration-component', require('./members/EventRegistration.vue').default);
Vue.component('default-registration', require('./members/DefaultRegistration.vue').default);
Vue.component('non-member-registration', require('./members/NonMemberRegistration.vue').default);
Vue.component('members-accept-mail', require('./members/AcceptMail.vue').default);
Vue.component('members-change-password', require('./members/ChangePassword.vue').default);

Vue.component('members-profile', require('./members/Profile.vue').default);


Vue.component('bs4-modal', require('./components/Bs4Modal.vue').default);
Vue.component('timer', require('./components/Timer.vue').default);
Vue.component('page5', require('./members/includes/Page5.vue').default);







Vue.component('modal-component', require('./components/Modal.vue').default);

Vue.filter('title_render', function (title) {
	if(title.length > 32){
  		return title.substring(0,32)+'...'
	}else{
		return title
	}
})

Vue.filter('title_render_public', function (title) {
	if(title.length > 11){
  		return title.substring(0,11)+'...'
	}else{
		return title
	}
})

Vue.filter('rate_category_render', function (title) {
	if(title.length > 20){
  		return title.substring(0,20)+'...'
	}else{
		return title
	}
})

Vue.filter('additional_category_render', function (title) {
	if(title.length > 20){
  		return title.substring(0,20)+'...'
	}else{
		return title
	}
})


Vue.filter('two_decimal', function (number) {
	return number.toFixed(2)
})
/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
    store,
});
