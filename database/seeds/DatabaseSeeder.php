<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        DB::table('settings')->insert([
            'c19_start_period' => '',
            'c19_end_period' => '',
        ]);        
    }
}
