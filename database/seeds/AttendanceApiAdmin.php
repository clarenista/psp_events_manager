<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;

class AttendanceApiAdmin extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $apikey = base64_encode(Str::random(40));

        \App\User::create([
            'first_name' => 'api', 'last_name' => 'api', 'contact' => '123', 'affiliation' => 'api', 'is_active' => 1, 'email' => 'api@psp.com.ph', 'role' => 4,  'username' => 'attendanceapi', 'password' => Hash::make('psp@dmin23#'),  'api_token' => $apikey

        ]);    }
}
