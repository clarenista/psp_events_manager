<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRegistrationDeleteDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('registration_delete_details', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('pspevent_id');
            $table->integer('user_id');
            $table->integer('rate_id');
            $table->integer('accommodation_id')->nullable();
            $table->text('order_reference_id')->nullable();
            $table->text('uid')->nullable();
            $table->string('payment_method');
            $table->integer('is_accepted')->default(0);
            $table->text('bpi_file_path')->nullable();
            $table->text('discount')->nullable();
            $table->string('discount_type')->nullable();
            $table->string('discount_percent')->nullable();
            $table->string('total');    
            $table->text('qrcode')->nullable();                        
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('registration_delete_details');
    }
}
