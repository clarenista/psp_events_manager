<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateC19OCRegistrantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('c19_o_c_registrants', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('fname');
            $table->string('mname');
            $table->string('lname');
            $table->string('email');
            $table->string('contact');
            $table->string('dob');
            $table->string('specialty');
            $table->string('classification');
            $table->string('affiliation');
            $table->string('cMembership');
            $table->text('purpose');
            $table->string('triedOnlineCourse');
            $table->string('levelsOfComputerLiteracy');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('c19_o_c_registrants');
    }
}
