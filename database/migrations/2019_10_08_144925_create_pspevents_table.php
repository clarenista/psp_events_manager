<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePspeventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pspevents', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title');
            $table->date('start_dt');
            $table->date('end_dt');
            $table->string('start_tm');
            $table->string('end_tm');
            $table->text('venue');
            $table->text('description');
            $table->text('uploaded_description_file')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pspevents');
    }
}
