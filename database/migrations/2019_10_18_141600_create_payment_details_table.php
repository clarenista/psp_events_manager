<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePaymentDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment_details', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('pspevent_id');
            $table->integer('user_id');
            $table->integer('rate_id');
            $table->integer('accommodation_id')->nullable();
            $table->text('order_reference_id')->nullable();
            $table->text('uid')->nullable();
            $table->string('payment_method');
            $table->integer('is_accepted')->default(0);
            $table->text('bpi_file_path')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payment_details');
    }
}
