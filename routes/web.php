<?php

use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\payment_details;
use App\Exports\PaymentExport;
use Maatwebsite\Excel\Facades\Excel;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@home');

// Route::get('/export_payment/{id}', function (Request $request) {
// 	// $payment =  payment_details::has('member_details')->get();
// 	return Excel::download( new PaymentExport($request->id), 'registrants.xlsx') ;
// 	return $payment->member_details;
// });

Route::get('/login', 'AuthController@login')->name('login');
Route::post('/login', 'AuthController@authenticate');
Route::get('/sign-up', 'AuthController@register');
Route::post('/sign-up', 'AuthController@saveMember');
Route::get('/password-remind', 'AuthController@passwordRemind');
Route::post('/password-remind', 'AuthController@sendResetPassword');
Route::get('/reset-password/{reset_password_key}', 'AuthController@resetPassword');
Route::post('/reset-password/{reset_password_key}', 'AuthController@saveNewPassword');
// Route::get('/test', function(){
// 	$qrcode = 'test';
// 	return view('emails.acceptmembermail', ['qrcode' => $qrcode]);
// });

Route::get('/settings', 'SettingController@fetchSettings');

Route::get('/MolPath-online-course', 'C19OCRegistrantController@index');
Route::get('/MolPath-online-course-residents', 'C19OCRegistrantController@residentsIndex');

Route::post('/MolPath-online-course', 'C19OCRegistrantController@saveRegistrant');
Route::post('/MolPath-online-course-residents', 'C19OCRegistrantController@saveResidentRegistrant');

// Auth::routes(['register' => false]);
Route::get('/logout', "AuthController@logout");

// user modules
Route::group(['middleware' => ['auth']], function () {
	Route::get('/events', 'PspeventController@index');
	Route::get('/events/{id}', 'PspeventController@show')->name('showEvent');
	Route::post('/checkVoucherCode', 'VoucherController@checkCode');
});

// super admin & admin modules
Route::group(['middleware' => ['superadmin', 'auth']], function () {
	// event participants
	Route::get('/events/{event_id}/participants/{member_id}/accept', 'PaymentDetailsController@acceptMember')->name('acceptMember');

	// admin's user event registration
	Route::get('/events/{event_id}/members', 'PspeventController@unregistered');
	Route::get('/events/{event_id}/email', 'PspeventController@emailRegistrants');
	// Route::get('/events/{event_id}/preview_email', function () {
	// 	$registrant = null;
	// 	$registrant['first_name'] = 'first_name';
	// 	$registrant['last_name'] = 'last_name';
	// 	$registrant['email'] = 'email';
	// 	return view('emails.mailNonMemberRegistrantEvaluation', compact('registrant'));
	// });
	Route::post('/events/{event_id}/members/search', 'PspeventController@searchUnregistered');
	Route::get('/events/{event_id}/register/{user_id}', 'PspeventController@adminRegisterUser');
	// Route::post('/events/{event_id}/register/{user_id}', 'PaymentDetailsController@proceed_registration');
	Route::get('/events/{event_id}/new-user', 'PspeventController@newUser');
	Route::post('/events/{event_id}/new-user', 'PspeventController@saveNewUser');


	// password hash generation
	// Route::get('/hash/{string}', function($string){
	// 	return Hash::make($string);
	// });

	Route::get('/email-registrants', 'C19OCRegistrantController@email');

	// eval email
	Route::get('/events/{event_id}/email-evaluation', 'PspeventController@emailEvaluation');
	Route::get('/events/{event_id}/email-non-member-evaluation', 'PspeventController@emailNonMemberEvaluation');

	Route::get('/settings/psp', 'SettingController@psp')->name('mf_index');
	Route::get('/settings/psp/create-mf', 'SettingController@create_mf')->name('new_mf');
	Route::post('/settings/psp/store-mf', 'SettingController@store_mf')->name('store_mf');
	Route::get('/settings/psp/mf/{id}', 'SettingController@edit_mf')->name('edit_mf');
	Route::put('/settings/psp/mf/{id}', 'SettingController@update_mf')->name('update_mf');
	Route::delete('/settings/psp/mf/{id}/delete', 'SettingController@delete_mf')->name('delete_mf');
});

// super admin & admin modules
Route::group(['middleware' => ['admin', 'auth']], function () {

	Route::get('/c19-pcr', 'C19OCRegistrantController@registrants');
	Route::post('/payment_details/updateTransactionNumber', 'PaymentDetailsController@updateTransactionNumber');
	Route::post('/payment_details/updateTransactionDate', 'PaymentDetailsController@updateTransactionDate');
	// event basic details 
	Route::post('/events/save', 'PspeventController@store');
	Route::post('/events/update', 'PspeventController@update');

	Route::post('/events/search', 'PspeventController@search');

	Route::post('/events/{event_id}/additional/save', 'AdditionalFeeController@store');
	// event rates
	Route::post('/events/{event_id}/rates/save', 'RateController@store');
	Route::get('/events/{event_id}/rates/{rate_id}', 'RateController@show');
	Route::post('/events/{event_id}/rates/{rate_id}', 'RateController@update');

	// event additional
	Route::post('/events/{event_id}/additional/save', 'AdditionalFeeController@store');
	Route::get('/events/{event_id}/additional/{addtnl_id}', 'AdditionalFeeController@show');
	Route::post('/events/{event_id}/additional/{addtnl_id}', 'AdditionalFeeController@update');

	// event vouchers
	Route::post('/events/{event_id}/vouchers/save', 'VoucherController@store');
	Route::get('/events/{event_id}/vouchers/{voucher_id}', 'VoucherController@show');
	Route::post('/events/{event_id}/vouchers/{voucher_id}', 'VoucherController@update');

	// event accommodations
	Route::post('/events/{event_id}/accommodations/save', 'AccommodationController@store');
	Route::get('/events/{event_id}/accommodations/{accommodation_id}', 'AccommodationController@show');
	Route::post('/events/{event_id}/accommodations/{accommodation_id}', 'AccommodationController@update');

	// event participants
	Route::get('/events/{event_id}/participants', 'PaymentDetailsController@show');
	Route::post('/events/{event_id}/sort-participants', 'PaymentDetailsController@sort');
	Route::get('/events/{event_id}/participants/export', 'PaymentDetailsController@export');

	Route::get('/events/{event_id}/export_participants', 'PaymentDetailsController@export_participants');
	
	Route::get('/events/{event_id}/attendance', 'Admin\AttendanceController@index')->name('admin.attendance.index');

	// Route::get('/events/{event_id}/participants/{member_id}/accept', 'PaymentDetailsController@acceptMember');

	// members
	Route::get('/members', 'MemberController@index');
	Route::post('/members', 'MemberController@update');
	// Route::get('/members/{id}/edit', 'MemberController@edit')
	Route::post('/members/search', 'MemberController@search');
	Route::get('/members/{id}/verify', 'MemberController@verify');

	Route::get('/members/paginate', "MemberController@paginate");

	Route::post('/signInAs', 'MemberController@signInAs')->name('signInAs');

	// password hash generation
	// Route::get('/hash/{string}', function($string){
	// 	return Hash::make($string);
	// });

	// update c19 periods
	Route::post('/settings/update', "SettingController@update");
	Route::get('/registrations/export_mapping', 'C19OCRegistrantController@export_mapping')->name('registrations.export_mapping');
});
Route::get('/verification/{verification_cd}', 'MemberController@verifysuccess');

// user modules
Route::group(['middleware' => ['user', 'auth']], function () {
	// Route::get('/members/events/', 'PspeventController@user_index');
	// Route::get('/members/events/{id}', 'PspeventController@user_show');
	Route::get('/events/{event_id}/register', 'PspeventController@register_user');
	Route::get('/events/{event_id}/administrator-register-user', 'PspeventController@admin_register_user')->name('admin-register-user');
	// Route::post('/events/{event_id}/register/redirect', function(Request $request){
	// 	return 'test';
	// });
	Route::post('/events/{event_id}/register', 'PaymentDetailsController@proceed_registration');
	Route::post('/events/{event_id}/administrator-register-user', 'PaymentDetailsController@admin_proceed_registration');
	Route::post('/redirect', 'PaymentDetailsController@redirect')->name('redirect');
	Route::post('/events/{event_id}/upload-receipt', 'PaymentDetailsController@upload_receipt');


	Route::get('/profile', 'ProfileController@index');
	Route::post('/profile', 'ProfileController@updateProfile');

	Route::get('/profile/change-password', 'ProfileController@showPassword');
	Route::post('/profile/change-password', 'ProfileController@updatePassword');

	Route::post('/events/{event_id}/delete-registration', "PaymentDetailsController@deleteRegistration");
});



Route::get('/home', 'HomeController@index')->name('home');
Route::get('/dashboard', 'HomeController@dashboard')->name('dashboard');

// Route::post('/android/getPaymentDetails', function(Request $request){
// 	$payment_details = payment_details::where('qrcode', $request->qrcode)->first();
// 	if($payment_details != null){
// 		$user = App\User::find($payment_details->user_id);
// 		$event = App\pspevent::find($payment_details->pspevent_id);
// 		$payment_details->name = $user->first_name ." ".$user->last_name;
// 		$payment_details->email = $user->email;
// 		$payment_details->contact = $user->contact;
// 		$payment_details->classification = $user->classification;
// 		$payment_details->event_title = $event->title;
// 		$payment_details->status = 'success';
// 		return $payment_details;
// 	}else{
// 		return response()->json(['status' => 'failed']);
// 	}

// });

// Route::post('/android/getKit', function(Request $request){
// 	$payment_details = payment_details::where('qrcode', $request->qrcode)
// 										->update([
// 											'got_kit' => 1
// 										]);

// 	return response()->json(['status' => 'ok']);
// });
