<?php

use Illuminate\Http\Request;
use App\Http\Controllers\Api\AuthController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
// Route::post('user', 'AuthController@user');
// Route::post('user', function(){
//     return 'test';
// });
Route::middleware('client')->namespace('Api')->group(function () {
    Route::post('user', 'AuthController@user');
    Route::post('login', 'AuthController@portalLogin');
    Route::get('users', 'AuthController@users');
    Route::get('get-users', 'AuthController@getUsers');
    Route::post('/portal/users', 'AuthController@portalUsers');
    // Route::get('/password-remind', 'AuthController@passwordRemind');
    Route::post('/password-remind', 'AuthController@sendResetPassword');
    Route::post('/convention-password-remind', 'AuthController@conventionSendResetPassword');
    Route::post('/portal-password-remind', 'AuthController@portalSendResetPassword');
    // Route::get('/reset-password/{reset_password_key}', 'AuthController@resetPassword');
    Route::post('/reset-password', 'AuthController@saveNewPassword');
    //Routes
});


Route::post('mobile-login', 'Api\AuthController@mobileLogin');

Route::group(['middleware' => 'api'], function () {
    Route::post('save-attendance', 'Api\AttendanceController@store');

});


// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });
